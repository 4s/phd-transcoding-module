#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <gtest/gtest.h>
#include <nlohmann/json.hpp>

//using namespace s4::BasePlate;
//using namespace s4::messages;
using nlohmann::json;
using std::unique_ptr;
using std::shared_ptr;
using std::set;
using std::string;
using std::vector;




/**********************************************************************/
/*                                                                    */
/*                          Helper functions                          */
/*                                                                    */
/**********************************************************************/


/// Wait for about 1 second for the predicate to become true. 
bool await(std::function<bool()> predicate) {
  int current_iteration = 1;
  int max_iterations = 100;
  int milliseconds_sleep = 10;

  do {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds_sleep));
    if (current_iteration++ >= max_iterations) {
      return false;
    }
  }
  while(!predicate());

  return true;
}


/// True if s2 is contained in s1
bool contains(string s1, string s2) {
  return s1.find(s2) != std::string::npos;
}

/**********************************************************************/
/*                                                                    */
/*                             Unit tests                             */
/*                                                                    */
/**********************************************************************/

TEST(PHDTranscodingModule, DummyAlwaysSuccess) {
}

TEST(PHDTranscodingModule, DummyMustSucceed) {
  ASSERT_TRUE(1);
}
