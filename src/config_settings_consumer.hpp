
#ifndef CONFIG_SETTINGS_CONSUMER_HPP
#define CONFIG_SETTINGS_CONSUMER_HPP

#include "BasePlate-fix.hpp"

#include <memory>
#include <set>

namespace s4 {
namespace messages_support {

class ConfigSettingsConsumer {
public:
  virtual void announceStore(const s4::BasePlate::peer_t &sender) = 0;
  virtual ~ConfigSettingsConsumer() = default;
};

class ConfigSettingsConsumerEndpoint {

public:
  explicit ConfigSettingsConsumerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  void addConsumer(std::shared_ptr<ConfigSettingsConsumer> consumer);

  void solicitStore();
  void readConfiguration(s4::BasePlate::peer_t peer, std::string configurationName,
                         std::shared_ptr<s4::BasePlate::Response<std::map<std::string, std::string>>> response);
  void writeConfiguration(s4::BasePlate::peer_t peer, std::string configurationName,
                          std::map<std::string, std::string> configuration,
                          std::shared_ptr<s4::BasePlate::Response<>> response);

private:
  s4::BasePlate::EndpointFactoryTemporary &epFactory;
  std::set<std::shared_ptr<ConfigSettingsConsumer>> consumers;
};

} // namespace messages_support
} // namespace s4

#endif // CONFIG_SETTINGS_CONSUMER_HPP
