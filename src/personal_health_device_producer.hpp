
#ifndef PHD_PRODUCER_HPP
#define PHD_PRODUCER_HPP

#include "BasePlate-fix.hpp"

#include <memory>
#include <set>

namespace s4 {
namespace messages_support {

class PersonalHealthDeviceProducer {
public:
  virtual void subscribeDeviceType(s4::BasePlate::peer_t sender, int mdcCode) = 0;
  virtual void unsubscribeDeviceType(s4::BasePlate::peer_t sender, int mdcCode) = 0;
  virtual ~PersonalHealthDeviceProducer() = default;
};

class PersonalHealthDeviceProducerEndpoint {

public:
  explicit PersonalHealthDeviceProducerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  void addProducer(std::shared_ptr<PersonalHealthDeviceProducer> producer);

private:
  std::set<std::shared_ptr<PersonalHealthDeviceProducer>> producers;
};

} // namespace messages_support
} // namespace s4

#endif // PHD_PRODUCER_HPP
