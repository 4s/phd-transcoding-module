
#ifndef FHIR_MEASUREMENT_PRODUCER_HPP
#define FHIR_MEASUREMENT_PRODUCER_HPP

#include "BasePlate-fix.hpp"

#include <memory>

namespace s4 {
namespace messages_support {

class FHIRMeasurementProducerEndpoint {

public:
  explicit FHIRMeasurementProducerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  void measurementReceived(int deviceType, std::string fhirObservation, std::string fhirDevice);

private:
  s4::BasePlate::EndpointFactoryTemporary &epFactory;
};

} // namespace messages_support
} // namespace s4

#endif // FHIR_MEASUREMENT_PRODUCER_HPP
