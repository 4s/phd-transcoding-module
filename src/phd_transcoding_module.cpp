/*
 *   Copyright 2020 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Personal Health Device Transcoding module.
 *
 * This module should be split into several modules, one translating
 * from the GATT device to the IEEE domain information model and one
 * for translating generic DIM objects to a FHIR bundle and/or FHIR
 * server upload.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2020 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

// Module public API
#include "phd-transcoding.hpp"
// Module internal subsystems
#include "controller.hpp"

// External dependencies
#include "BasePlate-fix.hpp"

#include "config_settings_consumer.hpp"
#include "device_control_consumer.hpp"
#include "fhir_measurement_producer.hpp"
#include "gatt_consumer.hpp"
#include "personal_health_device_producer.hpp"
#include "time_consumer.hpp"

#include "current_time_service.hpp"
#include "device_information_service.hpp"
#include "dim2fhir.hpp"
#include "glucose_service.hpp"

#include <memory>
#include <string>

// temporary
#include "Log.pb.h"
#include <sstream>

using s4::BasePlate::Context;
using s4::BasePlate::CoreError;
using s4::BasePlate::Function;
using s4::BasePlate::MetaData;
using s4::BasePlate::ModuleBase;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::dim2fhir::DIM2FHIRConverter;
using s4::messages_support::ConfigSettingsConsumerEndpoint;
using s4::messages_support::DeviceControlConsumerEndpoint;
using s4::messages_support::FHIRMeasurementProducerEndpoint;
using s4::messages_support::GATTConsumerEndpoint;
using s4::messages_support::PersonalHealthDeviceProducerEndpoint;
using s4::messages_support::TimeConsumerEndpoint;

using std::function;
using std::shared_ptr;
using std::string;

namespace s4 {
namespace phd_transcoding {

/**********************************************************************/
/*                                                                    */
/*            Declare PHD Transcoding module private parts            */
/*                                                                    */
/**********************************************************************/

class PHDTranscodingModule::PImpl : public s4::BasePlate::Logger, public s4::BasePlate::EndpointFactoryTemporary {
  friend class PHDTranscodingModule;

  ModuleBase &getParent() override;

private:
  explicit PImpl(PHDTranscodingModule &parent);

  // Parent object
  PHDTranscodingModule &parent;

  // Endpoint implementations
  PersonalHealthDeviceProducerEndpoint phdProducerEndpoint;
  FHIRMeasurementProducerEndpoint fhirMeasurementProducerEndpoint;
  DeviceControlConsumerEndpoint deviceControlConsumerEndpoint;
  TimeConsumerEndpoint timeConsumerEndpoint;
  ConfigSettingsConsumerEndpoint configSettingsConsumerEndpoint;
  GATTConsumerEndpoint gattConsumerEndpoint;

  // Subsystems
  DIM2FHIRConverter dim2fhirConverter;
  DeviceInformationService deviceInformationService;
  CurrentTimeService currentTimeService;
  GlucoseService glucoseService;
  Controller controller;

  /**********************************************************************/
  /*                                                                    */
  /*     Fix missing Baseplate implementation (cf BasePlate-fix.hpp)    */
  /*                                                                    */
  /*             (this section will disappear when the                  */
  /*              baseplate is fully implemented)                       */
  /*                                                                    */

  objId_t nextObjId = 0xFEEDABEEDEADBEEF;

  uint16_t nextCallbackId = 0;

  void log(s4::messages::classes::log::LogLevel level, string signal, string file, int line, string message) {
    auto msg = s4::messages::classes::log::Log();
    msg.set_module_tag("PHDTranscoding");
    msg.set_file_name(file);
    msg.set_line_number(line);
    msg.set_log_level(level);
    msg.set_message(message);
    parent.sendMulticast("Log", signal, &msg);
  }

  string createCallbackId() {
    uint16_t cid = nextCallbackId++;
    std::ostringstream stream;
    stream << std::uppercase << std::setfill('0') << std::setw(4) << std::hex << cid;
    return stream.str();
  }

  void cleanup(const string &topic, const string &signal) {
    if (signal == "ERR.LAST" || signal == "OK.LAST") {
      removeFunction(topic + "_OK.");
      removeFunction(topic + "_ERR.");
    }
  }

  class SuccessWrapper : public Function {
    friend class PImpl;
    Function *wrapped;
    string topic;
    PImpl &parent;
    SuccessWrapper(Function *wrapped, const string &topic, PImpl &parent)
        : Function(wrapped->expectsData()), wrapped(wrapped), topic(topic), parent(parent) {}

  public:
    ~SuccessWrapper() { delete wrapped; }
    google::protobuf::MessageLite *getInstance() override { return wrapped->getInstance(); }
    void apply(MetaData &metadata, google::protobuf::MessageLite *input, BasePlate::Callback &callback,
               BasePlate::ErrorCallback &error) override {
      string signal = metadata.signal;
      wrapped->apply(metadata, input, callback, error);
      parent.cleanup(topic, signal);
    }
  };

  class ErrorWrapper : public Function {
    friend class PImpl;
    function<void(MetaData, shared_ptr<CoreError>)> wrapped;
    string topic;
    PImpl &parent;
    ErrorWrapper(function<void(MetaData, shared_ptr<CoreError>)> wrapped, const string &topic, PImpl &parent)
        : Function(true), wrapped(wrapped), topic(topic), parent(parent) {}

  public:
    google::protobuf::MessageLite *getInstance() override { return new s4::messages::classes::error::Error(); }
    void apply(MetaData &metadata, google::protobuf::MessageLite *input,
               __attribute__((unused)) BasePlate::Callback &callback,
               __attribute__((unused)) BasePlate::ErrorCallback &error) override {
      string signal = metadata.signal;
      auto e = dynamic_cast<s4::messages::classes::error::Error *>(input);
      wrapped(metadata, shared_ptr<CoreError>(
                            new CoreError(e->module_tag(), e->message(), e->interface_name(), e->error_code())));
      parent.cleanup(topic, signal);
    }
  };

  string registerResponseUID(__attribute__((unused)) string interface, Function *successCallback,
                             function<void(MetaData, shared_ptr<CoreError>)> errorCallback) override {
    string callbackIdDecl = createCallbackId();
    string callbackIdInvoke = "#" + callbackIdDecl;

    addFunction(callbackIdInvoke + "_OK.", new SuccessWrapper(successCallback, callbackIdInvoke, *this));
    addFunction(callbackIdInvoke + "_ERR.", new ErrorWrapper(errorCallback, callbackIdInvoke, *this));
    return callbackIdDecl;
  }

  void addFunction(string interface, Function *function) override { parent.addFunction(interface, function); }

  void sendUnicastWithCallback(peer_t destination, string interface, string signal, string callbackInterface,
                               google::protobuf::MessageLite *arg = nullptr) const override {
    parent.sendUnicastWithCallback(destination, interface, signal, callbackInterface, arg);
  }

public:
  // Logger implementation
  void fatal(const string &file, int line, const string &message) override {
    log(s4::messages::classes::log::LogLevel::FATAL, "++++", file, line, message);
  }
  void error(const string &file, int line, const string &message) override {
    log(s4::messages::classes::log::LogLevel::ERROR, "+++", file, line, message);
  }
  // void warn(const string &file, int line, const string &message)  { log(s4::messages::classes::log::LogLevel::WARN,
  // "++", file, line, message); } void info(const string &file, int line, const string &message)  {
  // log(s4::messages::classes::log::LogLevel::INFO, "+", file, line, message); }
  void debug(const string &file, int line, const string &message) override {
    log(s4::messages::classes::log::LogLevel::DEBUG, "-", file, line, message);
  }

  objId_t createObjId() override { return nextObjId++; }

  // EndpointFactoryTemporary implementation
  void removeFunction(std::string interface) override { parent.removeFunction(interface); }
};

} // namespace phd_transcoding
namespace BasePlate {

peer_t handler2peer(int32_t handler) {
  std::ostringstream stream;
  stream << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << handler;
  return stream.str();
}

} // namespace BasePlate
namespace phd_transcoding {

/*                                                                    */
/*                                                                    */
/*   End of missing Baseplate implementation (cf BasePlate-fix.hpp)   */
/*                                                                    */
/**********************************************************************/

//
// Constructor
//

PHDTranscodingModule::PImpl::PImpl(PHDTranscodingModule &parent)
    : Logger(), parent(parent), phdProducerEndpoint(*this), fhirMeasurementProducerEndpoint(*this),
      deviceControlConsumerEndpoint(*this), timeConsumerEndpoint(*this), configSettingsConsumerEndpoint(*this),
      gattConsumerEndpoint(*this), dim2fhirConverter(fhirMeasurementProducerEndpoint, *this),
      deviceInformationService(configSettingsConsumerEndpoint, *this),
      currentTimeService(configSettingsConsumerEndpoint, timeConsumerEndpoint, *this),
      glucoseService(configSettingsConsumerEndpoint, timeConsumerEndpoint, *this),
      controller(phdProducerEndpoint, deviceControlConsumerEndpoint, configSettingsConsumerEndpoint,
                 timeConsumerEndpoint, gattConsumerEndpoint, dim2fhirConverter, deviceInformationService,
                 currentTimeService, glucoseService, *this) {}

ModuleBase &PHDTranscodingModule::PImpl::getParent() { return parent; }

/**********************************************************************/
/*                                                                    */
/*                 PHDTranscodingModule implementation                */
/*                                                                    */
/**********************************************************************/

//
// Constructor
//
PHDTranscodingModule::PHDTranscodingModule(Context &context)
    : ModuleBase(context, "10000000"), pImpl(new PImpl(*this)) {
  // Hack to get function responses due to missing baseplate
  // implementations.  The registered handler will never be
  // called, but the registration will ensure that dynamically
  // added callback handlers can be invoked.
  addFunction<s4::messages::classes::error::Error>("#", [](MetaData, s4::messages::classes::error::Error *) {});
  start();
}

} // namespace phd_transcoding
} // namespace s4
