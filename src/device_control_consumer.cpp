

#include "device_control_consumer.hpp"

#include "Device.pb.h"
#include "DeviceControl.pb.h"

using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using namespace s4::messages::interfaces;
using namespace s4::messages::classes;
using s4::BasePlate::EndpointFactoryTemporary;
using std::shared_ptr;
using std::string;

namespace s4 {
namespace messages_support {

DeviceControlConsumerEndpoint::DeviceControlConsumerEndpoint(EndpointFactoryTemporary &epFactory)
    : epFactory(epFactory) {

  epFactory.addFunction<device_control::P2C>(
      "DeviceControl.P2C", [this](BasePlate::MetaData metadata, device_control::P2C *p2c) -> void {
        if (p2c->has_event()) {
          auto event = p2c->event();
          if (event.has_search_initiated()) {
            device_control::SearchInitiated si = event.search_initiated();
            int count = si.found_so_far_size();
            for (int i = 0; i < count; i++) {
              device_control::TransportBundle tb = si.found_so_far(i);
              if (tb.has_device_transport()) {
                for (auto consumer : consumers) {
                  consumer->searchResult(metadata.sender, si.search_id(), tb.transport_persistent_id(),
                                         tb.device_transport());
                }
              }
            }
          } else if (event.has_search_result()) {
            device_control::SearchResult sr = event.search_result();
            if (sr.has_result()) {
              device_control::TransportBundle tb = sr.result();
              if (tb.has_device_transport()) {
                for (auto consumer : consumers) {
                  consumer->searchResult(metadata.sender, sr.search_id(), tb.transport_persistent_id(),
                                         tb.device_transport());
                }
              }
            }
          }
        }
      });
}

void DeviceControlConsumerEndpoint::addConsumer(shared_ptr<DeviceControlConsumer> consumer) {
  consumers.insert(consumer);
}

void DeviceControlConsumerEndpoint::search(objId_t searchId) {
  auto search = new device_control::Search();
  search->set_search_id(searchId);
  auto event = new device_control::C2P::Event();
  event->set_allocated_search(search);
  auto msg = device_control::C2P();
  msg.set_allocated_event(event);
  epFactory.getParent().sendMulticast("DeviceControl.C2P", "N/A", &msg);
}

void DeviceControlConsumerEndpoint::stopSearch(objId_t searchId) {
  auto stop = new device_control::StopSearch();
  stop->set_search_id(searchId);
  auto event = new device_control::C2P::Event();
  event->set_allocated_stop_search(stop);
  auto msg = device_control::C2P();
  msg.set_allocated_event(event);
  epFactory.getParent().sendMulticast("DeviceControl.C2P", "N/A", &msg);
}

} // namespace messages_support
} // namespace s4
