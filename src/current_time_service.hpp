
#ifndef CURRENT_TIME_SERVICE_HPP
#define CURRENT_TIME_SERVICE_HPP

#include "BasePlate-fix.hpp"

#include "config_settings_consumer.hpp"
#include "gatt_consumer.hpp"
#include "time_consumer.hpp"

#include <memory>

namespace s4 {
namespace phd_transcoding {

class CurrentTimeService {

public:
  CurrentTimeService(s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                     s4::messages_support::TimeConsumerEndpoint &timeConsumerEndpoint, s4::BasePlate::Logger &logger);
  ~CurrentTimeService();

private:
  __attribute__((unused)) s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint;
  __attribute__((unused)) s4::messages_support::TimeConsumerEndpoint &timeConsumerEndpoint;
  __attribute__((unused)) s4::BasePlate::Logger &logger;

  class EventHandler;
  std::shared_ptr<EventHandler> eventHandler;

  void announceStore(const s4::BasePlate::peer_t &sender);
  void announceClock(const s4::BasePlate::peer_t &sender);
};

} // namespace phd_transcoding
} // namespace s4

#endif // CURRENT_TIME_SERVICE_HPP
