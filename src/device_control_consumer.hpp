
#ifndef DEVICE_CONTROL_CONSUMER_HPP
#define DEVICE_CONTROL_CONSUMER_HPP

#include "BasePlate-fix.hpp"
// FIXME: Remove this dependency
#include "Device.pb.h"

#include <memory>
#include <set>

namespace s4 {
namespace messages_support {

class DeviceControlConsumer {
public:
  virtual void searchResult(s4::BasePlate::peer_t sender, s4::BasePlate::objId_t searchId, std::string tpid,
                            s4::messages::classes::device::DeviceTransport device) = 0;
  virtual ~DeviceControlConsumer() = default;
};

class DeviceControlConsumerEndpoint {

public:
  explicit DeviceControlConsumerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  void addConsumer(std::shared_ptr<DeviceControlConsumer> consumer);

  void search(s4::BasePlate::objId_t searchId);
  void stopSearch(s4::BasePlate::objId_t searchId);

private:
  s4::BasePlate::EndpointFactoryTemporary &epFactory;
  std::set<std::shared_ptr<DeviceControlConsumer>> consumers;
};

} // namespace messages_support
} // namespace s4

#endif // DEVICE_CONTROL_CONSUMER_HPP
