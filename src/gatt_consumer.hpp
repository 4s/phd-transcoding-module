
#ifndef GATT_CONSUMER_HPP
#define GATT_CONSUMER_HPP

#include "BasePlate-fix.hpp"
#include "bluetooth_utilities.hpp"

#include <list>
#include <memory>
#include <set>
#include <vector>

namespace s4 {
namespace messages_support {

class GATTServerProxy;
class GATTServiceProxy;
class GATTCharacteristicProxy;
typedef std::string GATTDescriptorProxy; // FIXME: Descriptors not yet supported

enum class GATTDisconnectedReason : int {
  MY_DISCONNECT = 0,
  OUT_OF_RANGE = 1,
  PEER_DISCONNECT = 2,
  TIMEOUT = 3,
  IO_ERROR = 4
};

class GATTConsumerEndpoint {
  friend class GATTServerProxy;
  friend class GATTServiceProxy;
  friend class GATTCharacteristicProxy;

public:
  explicit GATTConsumerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  std::shared_ptr<GATTServerProxy>
  createServerProxy(s4::BasePlate::peer_t handler, s4::BasePlate::objId_t handle,
                    std::function<void(std::shared_ptr<GATTServerProxy>, GATTDisconnectedReason)> disconnectedCallback);

private:
  s4::BasePlate::EndpointFactoryTemporary &epFactory;
  std::map<s4::BasePlate::objId_t, std::shared_ptr<GATTServerProxy>> gattServers;
  std::map<s4::BasePlate::objId_t, std::function<void(std::vector<uint8_t>)>> notificationHandlers;

  void connect(s4::BasePlate::peer_t handler, s4::BasePlate::objId_t handle,
               std::shared_ptr<s4::BasePlate::Response<>> response);
  void disconnect(s4::BasePlate::peer_t handler, s4::BasePlate::objId_t handle);
  void
  discoverServices(GATTServerProxy &serverProxy, BluetoothUUID *uuid,
                   std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTServiceProxy>>>> response);
  void
  findIncludedServices(GATTServiceProxy &serviceProxy,
                       std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTServiceProxy>>>> response);
  void discoverCharacteristics(
      GATTServiceProxy &serviceProxy, BluetoothUUID *uuid,
      std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTCharacteristicProxy>>>> response);
  void discoverDescriptors(
      GATTCharacteristicProxy &characteristicProxy,
      std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTDescriptorProxy>>>> response);
  void readValue(GATTCharacteristicProxy &characteristicProxy,
                 std::shared_ptr<s4::BasePlate::Response<std::vector<uint8_t>>> response);
  void writeValueWithResponse(GATTCharacteristicProxy &characteristicProxy, std::vector<uint8_t> value,
                              std::shared_ptr<s4::BasePlate::Response<>> response);
  void writeValueWithoutResponse(GATTCharacteristicProxy &characteristicProxy, std::vector<uint8_t> value,
                                 std::shared_ptr<s4::BasePlate::Response<>> response);
  void setNotification(GATTCharacteristicProxy &characteristicProxy, bool enable,
                       std::function<void(std::vector<uint8_t>)> notificationCallback,
                       std::shared_ptr<s4::BasePlate::Response<>> response);
};

class GATTServerProxy {
  friend class GATTConsumerEndpoint;

public:
  void connect(std::shared_ptr<s4::BasePlate::Response<>> response);
  void disconnect();
  void
  discoverServices(BluetoothUUID *uuid,
                   std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTServiceProxy>>>> response);

private:
  GATTServerProxy(GATTConsumerEndpoint &endpoint, s4::BasePlate::peer_t handler, s4::BasePlate::objId_t handle,
                  std::function<void(std::shared_ptr<GATTServerProxy>, GATTDisconnectedReason)> disconnectedCallback);
  GATTConsumerEndpoint &endpoint;
  s4::BasePlate::peer_t handler;
  s4::BasePlate::objId_t handle;
  std::function<void(std::shared_ptr<GATTServerProxy>, GATTDisconnectedReason)> disconnectedCallback;
  std::map<s4::BasePlate::objId_t, std::shared_ptr<GATTServiceProxy>> services;
  std::map<s4::BasePlate::objId_t, std::shared_ptr<GATTCharacteristicProxy>> characteristics;
  std::map<s4::BasePlate::objId_t, std::shared_ptr<GATTDescriptorProxy>> descriptors;
};

class GATTServiceProxy {
  friend class GATTConsumerEndpoint;

public:
  const BluetoothUUID type;
  const bool primary;

  void
  findIncludedServices(std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTServiceProxy>>>> response);
  void discoverCharacteristics(
      BluetoothUUID *uuid,
      std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTCharacteristicProxy>>>> response);

private:
  GATTServiceProxy(GATTConsumerEndpoint &endpoint, GATTServerProxy &server, s4::BasePlate::objId_t handle,
                   BluetoothUUID type, bool primary);
  GATTConsumerEndpoint &endpoint;
  GATTServerProxy &server;
  s4::BasePlate::objId_t handle;
  std::map<s4::BasePlate::objId_t, std::shared_ptr<GATTServiceProxy>> includedServices;
};

class GATTCharacteristicProxy {
  friend class GATTConsumerEndpoint;

public:
  const BluetoothUUID type;
  const bool canBroadcast;
  const bool canRead;
  const bool canWriteWithoutResponse;
  const bool canWriteWithResponse;
  const bool canNotify;
  const bool canIndicate;
  const bool canAuthenticatedSignedWrite;
  const bool hasExtendedProperties;

  void discoverDescriptors(
      std::shared_ptr<s4::BasePlate::Response<std::list<std::shared_ptr<GATTDescriptorProxy>>>> response);
  void readValue(std::shared_ptr<s4::BasePlate::Response<std::vector<uint8_t>>> response);
  void writeValueWithResponse(std::vector<uint8_t> value, std::shared_ptr<s4::BasePlate::Response<>> response);
  void writeValueWithoutResponse(std::vector<uint8_t> value, std::shared_ptr<s4::BasePlate::Response<>> response);

  /// Must also update the CCC Descriptor
  void setNotification(bool enable, std::function<void(std::vector<uint8_t>)> notificationCallback,
                       std::shared_ptr<s4::BasePlate::Response<>> response);

private:
  GATTCharacteristicProxy(GATTConsumerEndpoint &endpoint, GATTServerProxy &server, s4::BasePlate::objId_t handle,
                          BluetoothUUID type, bool canBroadcast, bool canRead, bool canWriteWithoutResponse,
                          bool canWriteWithResponse, bool canNotify, bool canIndicate, bool canAuthenticatedSignedWrite,
                          bool hasExtendedProperties);
  GATTConsumerEndpoint &endpoint;
  GATTServerProxy &server;
  s4::BasePlate::objId_t handle;
};

} // namespace messages_support
} // namespace s4

#endif // GATT_CONSUMER_HPP
