

#include "bluetooth_utilities.hpp"

namespace s4 {
namespace messages_support {

bool operator==(const BluetoothUUID &lhs, const BluetoothUUID &rhs) {
  return lhs.mostSignificant == rhs.mostSignificant && lhs.leastSignificant == rhs.leastSignificant;
}

} // namespace messages_support
} // namespace s4
