
#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "BasePlate-fix.hpp"

#include "config_settings_consumer.hpp"
#include "device_control_consumer.hpp"
#include "gatt_consumer.hpp"
#include "personal_health_device_producer.hpp"
#include "time_consumer.hpp"

#include "current_time_service.hpp"
#include "device_information_service.hpp"
#include "dim2fhir.hpp"
#include "glucose_service.hpp"

#include <memory>

namespace s4 {
namespace phd_transcoding {

class Controller {

public:
  Controller(s4::messages_support::PersonalHealthDeviceProducerEndpoint &phdProducerEndpoint,
             s4::messages_support::DeviceControlConsumerEndpoint &deviceControlConsumerEndpoint,
             s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
             s4::messages_support::TimeConsumerEndpoint &timeConsumerEndpoint,
             s4::messages_support::GATTConsumerEndpoint &gattConsumerEndpoint,
             s4::dim2fhir::DIM2FHIRConverter &dim2fhirConverter, DeviceInformationService &deviceInformationService,
             CurrentTimeService &currentTimeService, GlucoseService &glucoseService, s4::BasePlate::Logger &logger);
  ~Controller();

private:
  s4::messages_support::DeviceControlConsumerEndpoint &deviceControlConsumerEndpoint;
  s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint;
  s4::messages_support::TimeConsumerEndpoint &timeConsumerEndpoint;
  s4::messages_support::GATTConsumerEndpoint &gattConsumerEndpoint;
  s4::dim2fhir::DIM2FHIRConverter &dim2fhirConverter;
  DeviceInformationService &deviceInformationService;
  __attribute__((unused)) CurrentTimeService &currentTimeService;
  GlucoseService &glucoseService;
  s4::BasePlate::Logger &logger;

  class EventHandler;
  std::shared_ptr<EventHandler> eventHandler;

  s4::BasePlate::objId_t searchid;
  bool deviceFound;

  std::shared_ptr<s4::dim::MDS> device;

  s4::BasePlate::peer_t clock, configStore;

  std::shared_ptr<s4::messages_support::GATTServiceProxy> genericAccessSP;
  std::shared_ptr<s4::messages_support::GATTServiceProxy> deviceInformationSP;
  std::shared_ptr<s4::messages_support::GATTServiceProxy> currentTimeSP;
  std::shared_ptr<s4::messages_support::GATTServiceProxy> glucoseSP;

  void subscribeDeviceType(s4::BasePlate::peer_t sender, int mdcCode);
  void unsubscribeDeviceType(s4::BasePlate::peer_t sender, int mdcCode);
  void searchResult(s4::BasePlate::peer_t sender, s4::BasePlate::objId_t search_id, std::string tpid,
                    s4::messages::classes::device::DeviceTransport device);
  void announceStore(const s4::BasePlate::peer_t &sender);
  void announceClock(const s4::BasePlate::peer_t &sender);

  void handleConnectionSuccess(std::shared_ptr<s4::messages_support::GATTServerProxy> serverProxy,
                               uint64_t transportAddress);
  void handleConnectionFailed(std::shared_ptr<s4::messages_support::GATTServerProxy> serverProxy,
                              std::shared_ptr<s4::BasePlate::CoreError> e);
  void receiveGlucoseObservations(std::shared_ptr<s4::messages_support::GATTServerProxy> serverProxy);
};

} // namespace phd_transcoding
} // namespace s4

#endif // CONTROLLER_HPP
