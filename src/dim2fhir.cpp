
#include "dim2fhir.hpp"

using s4::BasePlate::CoreError;
using s4::messages_support::FHIRMeasurementProducerEndpoint;
using std::set;
using std::shared_ptr;
using std::string;
using namespace s4::dim;

namespace s4 {
namespace dim2fhir {

DIM2FHIRConverter::DIM2FHIRConverter(FHIRMeasurementProducerEndpoint &fhirEndpoint, s4::BasePlate::Logger &logger)
    : fhirEndpoint(fhirEndpoint), logger(logger) {}

string templateObservation();
string templateDevice();

string int2hex(uint64_t value, uint8_t digits) {
  std::stringstream s;
  s << std::setfill('0') << std::setw(digits) << std::hex << value;
  return s.str();
}

void DIM2FHIRConverter::convertScanReport(const MDS &mdsObject, const set<DIMEntity *> &reportedObjects) {

  for (auto dimEntity : reportedObjects) {
    Numeric *obsObject = dynamic_cast<Numeric *>(dimEntity);
    // We currently skip non-numeric objects...
    if (!obsObject)
      continue;

    string phdEUI64 = int2hex(mdsObject.systemId, 16);
    string phdEUI64dashed = phdEUI64.substr(0, 2) + "-" + phdEUI64.substr(2, 2) + "-" + phdEUI64.substr(4, 2) + "-" +
                            phdEUI64.substr(6, 2) + "-" + phdEUI64.substr(8, 2) + "-" + phdEUI64.substr(10, 2) + "-" +
                            phdEUI64.substr(12, 2) + "-" + phdEUI64.substr(14, 2);
    string phdBtMac = int2hex(mdsObject.transportId, 12);
    string phdManufacturer = mdsObject.manufacturer;
    string phdModel = mdsObject.modelNumber;
    double obsValue = obsObject->value;
    int obsValuePrecision = obsObject->precision;
    string obsTime = obsObject->dateAndTime;
    string obsTimeCompact =
        obsTime.substr(0, 4) + obsTime.substr(5, 2) + obsTime.substr(8, 5) + obsTime.substr(14, 2) +
        obsTime.substr(17, 2) +
        ".00"; // FIXME: This just copies the local time (purging time zone). It must use UTC time instead

    string phdSerial = "";
    for (auto prodSpec : mdsObject.productionSpecification) {
      // cppcheck insists that we use std::find_if below. But that would make it LESS readable and increase
      // complexity...
      // cppcheck-suppress useStlAlgorithm
      if (prodSpec.specType == MDS::ProdSpec::serialNumber) {
        phdSerial = prodSpec.prodSpec;
        break;
      }
    }

    char buffer[3000];

    sprintf(buffer, templateObservation().c_str(), phdEUI64dashed.c_str(), obsValuePrecision, obsValue,
            obsTimeCompact.c_str(), obsTime.c_str(), obsValuePrecision, obsValue);
    string observation(buffer);

    sprintf(buffer, templateDevice().c_str(), phdEUI64.c_str(), phdBtMac.c_str(), phdEUI64dashed.c_str(),
            phdManufacturer.c_str(), phdModel.c_str(), phdSerial.c_str());
    string device(buffer);

    fhirEndpoint.measurementReceived(0x00081011, observation, device);
  }
}

string templateObservation() {
  return R"1234({
  "resourceType": "Observation",
  "meta": {
    "profile": [
      "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdNumericObservation"
    ]
  },
  "identifier": [
    {
      "value": "--%s-160184-%.*f-%s"
    }
  ],
  "status": "final",
  "code": {
    "coding": [
      {
        "system": "urn:iso:std:iso:11073:10101",
        "code": "160184",
        "display": "MDC_CONC_GLU_CAPILLARY_WHOLEBLOOD"
      }
    ],
    "text": "MDC_CONC_GLU_CAPILLARY_WHOLEBLOOD: Glucose, capillary blood"
  },
  "effectiveDateTime": "%s",
  "valueQuantity": {
    "value": %.*f,
    "system": "http://unitsofmeasure.org",
    "code": "mmol/L",
    "unit": "mmol/L"
  }
})1234";
}

string templateDevice() {
  return R"1234({
  "resourceType": "Device",
  "id": "%s.%s",
  "meta": {
    "profile": [
      "http://hl7.org/fhir/uv/phd/StructureDefinition/PhdDevice"
    ]
  },
  "identifier": [
    {
      "type": {
        "coding": [{
          "system": "http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers",
          "code": "SYSID"
        }]
      },
      "system": "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680",
      "value": "%s"
    }
  ],
  "type": {
    "coding": [
      {
        "system": "urn:iso:std:iso:11073:10101",
        "code": "65573",
        "display": "MDC_MOC_VMS_MDS_SIMP"
      }
    ],
    "text": "MDC_MOC_VMS_MDS_SIMP: Continua Personal Health Device"
  },
  "specialization": [
    {
      "systemType": {
        "coding": [
          {
            "system": "urn:iso:std:iso:11073:10101",
            "code": "528401",
            "display": "MDC_DEV_SPEC_PROFILE_GLUCOSE"
          }
        ],
        "text": "MDC_DEV_SPEC_PROFILE_GLUCOSE: Glucose meter"
      },
      "version": "1"
    }
  ],
  "manufacturer": "%s",
  "modelNumber": "%s",
  "serialNumber": "%s"
})1234";
}

} // namespace dim2fhir
} // namespace s4
