

#include "config_settings_consumer.hpp"

#include "ConfigSettings.pb.h"

using s4::BasePlate::CoreError;
using s4::BasePlate::EndpointFactoryTemporary;
using s4::BasePlate::MetaData;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using namespace s4::messages::interfaces;
using namespace s4::messages::classes;
using std::map;
using std::shared_ptr;
using std::string;

namespace s4 {
namespace messages_support {

ConfigSettingsConsumerEndpoint::ConfigSettingsConsumerEndpoint(EndpointFactoryTemporary &epFactory)
    : epFactory(epFactory) {

  epFactory.addFunction<config_settings::P2C>("ConfigSettings.P2C",
                                              [this](MetaData metadata, config_settings::P2C *p2c) -> void {
                                                if (p2c->has_event()) {
                                                  auto event = p2c->event();
                                                  if (event.has_announce_store()) {
                                                    for (auto consumer : consumers)
                                                      consumer->announceStore(metadata.sender);
                                                  }
                                                }
                                              });
}

void ConfigSettingsConsumerEndpoint::addConsumer(shared_ptr<ConfigSettingsConsumer> consumer) {
  consumers.insert(consumer);
}

void ConfigSettingsConsumerEndpoint::solicitStore() {
  auto solicit = new config_settings::SolicitStore();
  auto event = new config_settings::C2P::Event();
  event->set_allocated_solicit_store(solicit);
  auto msg = config_settings::C2P();
  msg.set_allocated_event(event);
  epFactory.getParent().sendMulticast("ConfigSettings.C2P", "N/A", &msg);
}

void ConfigSettingsConsumerEndpoint::readConfiguration(peer_t peer, string configurationName,
                                                       shared_ptr<Response<map<string, string>>> response) {
  auto read = new config_settings::ReadConfiguration();
  read->set_configuration_name(configurationName);
  auto request = new config_settings::C2P::Request();
  request->set_allocated_read_configuration(read);
  auto msg = config_settings::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<config_settings::ReadConfigurationSuccess>(
      peer, "ConfigSettings.C2P",
      [response](MetaData metadata, config_settings::ReadConfigurationSuccess *resp) -> void {
        map<string, string> target;
        auto source = resp->configuration();
        for (auto kvPair : source) {
          target[kvPair.key()] = kvPair.value();
        }
        response->success(metadata.signal == "OK.MORE", target);
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void ConfigSettingsConsumerEndpoint::writeConfiguration(peer_t peer, string configurationName,
                                                        map<string, string> configuration,
                                                        shared_ptr<Response<>> response) {
  auto write = new config_settings::WriteConfiguration();
  write->set_configuration_name(configurationName);
  for (auto source : configuration) {
    auto target = write->add_configuration();
    target->set_key(source.first);
    target->set_value(source.second);
  }
  auto request = new config_settings::C2P::Request();
  request->set_allocated_write_configuration(write);
  auto msg = config_settings::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<config_settings::WriteConfigurationSuccess>(
      peer, "ConfigSettings.C2P",
      [response](MetaData metadata, __attribute__((unused)) config_settings::WriteConfigurationSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE");
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

} // namespace messages_support
} // namespace s4
