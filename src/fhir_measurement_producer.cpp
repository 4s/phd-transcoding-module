

#include "fhir_measurement_producer.hpp"

#include "FHIRMeasurement.pb.h"

using namespace s4::messages::interfaces;
using std::string;

namespace s4 {
namespace messages_support {

FHIRMeasurementProducerEndpoint::FHIRMeasurementProducerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory)
    : epFactory(epFactory) {}

void FHIRMeasurementProducerEndpoint::measurementReceived(int deviceType, string fhirObservation, string fhirDevice) {
  auto mr = new fhir_measurement::MeasurementReceived();
  mr->set_device_type(std::to_string(deviceType));
  mr->set_fhir_observation(fhirObservation);
  mr->set_fhir_device(fhirDevice);
  auto event = new fhir_measurement::P2C::Event();
  event->set_allocated_measurement_received(mr);
  auto msg = fhir_measurement::P2C();
  msg.set_allocated_event(event);
  epFactory.getParent().sendMulticast("FHIRMeasurement.P2C", "N/A", &msg);
}

} // namespace messages_support
} // namespace s4
