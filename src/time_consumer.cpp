

#include "time_consumer.hpp"

#include "Time.pb.h"

using s4::BasePlate::CoreError;
using s4::BasePlate::EndpointFactoryTemporary;
using s4::BasePlate::MetaData;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using namespace s4::messages::interfaces;
using namespace s4::messages::classes;
using std::shared_ptr;
using std::string;
using format_t = s4::messages_support::TimeConsumerEndpoint::format_t;

namespace s4 {
namespace messages_support {

TimeConsumerEndpoint::TimeConsumerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory) : epFactory(epFactory) {

  epFactory.addFunction<time::P2C>("Time.P2C", [this](MetaData metadata, time::P2C *p2c) -> void {
    if (p2c->has_event()) {
      auto event = p2c->event();
      if (event.has_announce_clock()) {
        for (auto consumer : consumers)
          consumer->announceClock(metadata.sender);
      }
    }
  });
}

void TimeConsumerEndpoint::addConsumer(shared_ptr<TimeConsumer> consumer) { consumers.insert(consumer); }

void TimeConsumerEndpoint::solicitClock() {
  auto solicit = new time::SolicitClock();
  auto event = new time::C2P::Event();
  event->set_allocated_solicit_clock(solicit);
  auto msg = time::C2P();
  msg.set_allocated_event(event);
  epFactory.getParent().sendMulticast("Time.C2P", "N/A", &msg);
}

void TimeConsumerEndpoint::startTimer(peer_t peer, uint32_t delayMillis, bool repeat,
                                      shared_ptr<Response<int32_t, bool>> response) {
  auto startTimer = new time::StartTimer();
  startTimer->set_delay_millis(delayMillis);
  startTimer->set_repeat(repeat);
  auto request = new time::C2P::Request();
  request->set_allocated_start_timer(startTimer);
  auto msg = time::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<time::StartTimerSuccess>(
      peer, "Time.C2P",
      [response](MetaData metadata, time::StartTimerSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE", resp->timer_id(), resp->fired());
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void TimeConsumerEndpoint::stopTimer(peer_t peer, int32_t timerId, shared_ptr<Response<>> response) {
  auto stopTimer = new time::StopTimer();
  stopTimer->set_timer_id(timerId);
  auto request = new time::C2P::Request();
  request->set_allocated_stop_timer(stopTimer);
  auto msg = time::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<time::StopTimerSuccess>(
      peer, "Time.C2P",
      [response](MetaData metadata, __attribute__((unused)) time::StopTimerSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE");
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void TimeConsumerEndpoint::readTime(peer_t peer, bool utc, format_t format,
                                    shared_ptr<Response<bool, TimeParts, string>> response) {
  auto readTime = new time::ReadTime();
  readTime->set_utc(utc);
  readTime->set_format(static_cast<messages::interfaces::time::ReadTime_Format>(format));
  auto request = new time::C2P::Request();
  request->set_allocated_read_time(readTime);
  auto msg = time::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<time::ReadTimeSuccess>(
      peer, "Time.C2P",
      [response](MetaData metadata, time::ReadTimeSuccess *resp) -> void {
        if (resp->has_parts()) {
          auto tp = resp->parts();
          response->success(metadata.signal == "OK.MORE", resp->utc(),
                            {tp.year(), tp.month(), tp.day(), tp.hour(), tp.min(), tp.sec(), tp.ms(), tp.timezone()},
                            "");
        } else {
          response->success(metadata.signal == "OK.MORE", resp->utc(), {0, 0, 0, 0, 0, 0, 0, 0}, resp->iso8601ext());
        }
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

} // namespace messages_support
} // namespace s4
