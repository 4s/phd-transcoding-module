

#include "glucose_service.hpp"
#include <cmath>
#include <ctime>

using s4::BasePlate::CoreError;
using s4::BasePlate::Logger;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using s4::dim::DIMEntity;
using s4::dim::Numeric;
using namespace s4::messages_support::BluetoothUUIDs;
using s4::messages_support::ConfigSettingsConsumer;
using s4::messages_support::ConfigSettingsConsumerEndpoint;
using s4::messages_support::GATTCharacteristicProxy;
using s4::messages_support::GATTServiceProxy;
using s4::messages_support::TimeConsumer;
using s4::messages_support::TimeConsumerEndpoint;
using std::function;
using std::list;
using std::make_shared;
using std::map;
using std::numeric_limits;
using std::set;
using std::shared_ptr;
using std::vector;

namespace s4 {
namespace phd_transcoding {

class GlucoseService::EventHandler : public ConfigSettingsConsumer, public TimeConsumer {
  friend class GlucoseService;

private:
  GlucoseService *gs;
  explicit EventHandler(GlucoseService *gs) : gs(gs) {}
  void kill() { gs = nullptr; }

public:
  void announceStore(const peer_t &sender) override {
    if (gs)
      gs->announceStore(sender);
  }
  void announceClock(const peer_t &sender) override {
    if (gs)
      gs->announceClock(sender);
  }
};

GlucoseService::GlucoseService(ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                               TimeConsumerEndpoint &timeConsumerEndpoint, Logger &logger)
    : configSettingsConsumerEndpoint(configSettingsConsumerEndpoint), logger(logger),
      eventHandler(new EventHandler(this)) {
  configSettingsConsumerEndpoint.addConsumer(eventHandler);
  timeConsumerEndpoint.addConsumer(eventHandler);
}

GlucoseService::~GlucoseService() { eventHandler->kill(); }

void GlucoseService::announceStore(const peer_t &sender) {
  if (configStore.empty())
    configStore = sender;
}

void GlucoseService::announceClock(const peer_t &sender) {
  if (clock.empty())
    clock = sender;
}

void sfloat2float(uint16_t mderSfloat, float *value, int8_t *precision) {
  *precision = 0;
  switch (mderSfloat) {
  case 0x07FF: // NaN
  case 0x0800: // NRes
  case 0x801:  // Error - Reserved value not allowed!
    *value = NAN;
    break;
  case 0x07FE: // +INF
    *value = INFINITY;
    break;
  case 0x802: // -INF
    *value = -INFINITY;
    break;
  default:

    // Convert to MDER FLOAT
    int8_t exponent = mderSfloat >> 8;
    exponent >>= 4; // Sign-extend
    int32_t mantissa = mderSfloat;
    mantissa <<= 20; // Sign-extend
    mantissa >>= 20;

    // Float return value
    *value = ((float)mantissa) * pow(10.0f, (float)exponent);

    // Precision return value
    *precision = -exponent;

    break;
  }
}

string timeAsISO(time_t &time) {
  char timestampBuffer[50];
  size_t len = strftime(timestampBuffer, 50, "%FT%T", localtime(&time));
  return string(timestampBuffer, len) + ((localtime(&time)->tm_isdst) ? "+02:00" : "+01:00");
  ;
}

string convertBOTimeToIsoString(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second,
                                int16_t offsetMinutes) {
  tm tm;
  tm.tm_year = year - 1900;
  tm.tm_mon = month - 1;
  tm.tm_mday = day;
  tm.tm_hour = hour;
  tm.tm_min = minute + offsetMinutes;
  tm.tm_sec = second;
  tm.tm_isdst = -1; // Auto-determine if DST was in effect at the provided time
  time_t time = std::mktime(&tm);
  return timeAsISO(time);
}

void GlucoseService::receiveNewObservations(shared_ptr<GATTServiceProxy> glucoseServiceProxy, string deviceId,
                                            function<void(set<DIMEntity *> &)> observationReceivedCallback) {

  // Create a context representing this progress/state of the async function call
  receiveNewObservationsContext_t context = make_shared<ReceiveNewObservationsContext>();
  context->glucoseObject = make_shared<Numeric>();
  context->glucoseServiceProxy = move(glucoseServiceProxy);
  context->observationReceivedCallback = move(observationReceivedCallback);
  context->nextSequenceNumber = 0xFFFF; // Start at the max value if nothing was stored in the Config Store
  context->deviceId = deviceId;
  context->freshMeasurement = false;

  // First step is to discover all characteristics

  // Read from Device Information Service
  context->glucoseServiceProxy->discoverCharacteristics(
      nullptr,
      make_shared<Response<list<shared_ptr<GATTCharacteristicProxy>>>>(
          [this, context](__attribute__((unused)) bool more,
                          list<shared_ptr<GATTCharacteristicProxy>> characteristics) -> void {
            // Identify and store all relevant characteristics
            identifyCharacteristics(characteristics, context);

            // Check that all mandatory characteristics were found
            if (!(context->glucoseMeasurement && context->glucoseMeasurementContext && context->glucoseFeature &&
                  context->recordAccessControlPoint)) {
              logger.ERROR("Failed to discover mandatory characteristics");
              return;
            }

            // Fetch the nextSequenceNumber - if it was previously stored

            // Set up characteristic reading (FIXME: Should only be read once and then cached in the module)
            enqueueReadConfiguration("phd_transcoding_glucose_sequence_numbers",
                                     [context](map<string, string> configuration) -> void {
                                       if (configuration.count(context->deviceId)) {
                                         context->nextSequenceNumber = std::stoi(configuration[context->deviceId]);
                                       }
                                     });
            enqueueCharacteristicReadValue(context->glucoseFeature, [this, context](vector<uint8_t> value) -> void {
              if (value.size() != 2) {
                logger.ERROR("Invalid Glucose Feature Characteristic value length");
              } else {
                context->featureFlags = (((uint16_t)(value[1])) << 8) | (value[0] & 0x00ff);
              }
            });
            enqueueCharacteristicSetNotification(
                context->glucoseMeasurement, true,
                [this, context](vector<uint8_t> value) -> void { handleGlucoseMeasurement(value, context); });
            enqueueCharacteristicSetNotification(
                context->glucoseMeasurementContext, true,
                [this, context](vector<uint8_t> value) -> void { handleGlucoseContextMeasurement(value, context); });
            enqueueCharacteristicSetNotification(
                context->recordAccessControlPoint, true,
                [this, context](vector<uint8_t> value) -> void { handleRACPResponse(value, context); });
            enqueueCharacteristicWriteValue(context->recordAccessControlPoint, [context]() -> vector<uint8_t> {
              if (context->nextSequenceNumber == 0xFFFF) {
                // First time we use this device. Read the last measurement to learn the sequence number
                return vector<uint8_t>{1, 6};
              } else {
                // Report stored records, >=, Sequence number: context->nextSequenceNumber
                return vector<uint8_t>{1, 3, 1, (uint8_t)(context->nextSequenceNumber & 0x00FF),
                                       (uint8_t)((context->nextSequenceNumber >> 8) & 0x00FF)};
              }
            });
          },
          [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
            logger.ERROR("Failed to read characteristics of the Glucose Service: " + e->message);
          }));
}

void GlucoseService::handleGlucoseMeasurement(vector<uint8_t> value, receiveNewObservationsContext_t context) {
  if (value.size() < 10) {
    logger.ERROR("Malformed glucose measurement");
    return;
  }
  uint8_t flags = value[0];
  bool hasTimeOffset = flags & 0x01;
  bool hasMeasurement = flags & 0x02;
  bool unitMilliMolePerL = flags & 0x04;
  bool hasAnnunciation = flags & 0x08;
  /* Currently unused: bool hasContext = flags & 0x10; */
  uint16_t sequenceNo = (((uint16_t)value[2]) << 8) | value[1];
  uint16_t year = (((uint16_t)value[4]) << 8) | value[3];
  uint8_t month = value[5];
  uint8_t day = value[6];
  uint8_t hour = value[7];
  uint8_t minute = value[8];
  uint8_t second = value[9];
  int16_t offsetMinutes = 0;
  float glucose = 0.0f;
  int8_t precision = 0;
  // The following three are not yet used - suppress warning from static analyzer
  // cppcheck-suppress variableScope
  // cppcheck-suppress unreadVariable
  uint8_t type = 0;
  // cppcheck-suppress variableScope
  // cppcheck-suppress unreadVariable
  uint8_t location = 0;
  // cppcheck-suppress variableScope
  uint16_t annunciation;
  uint8_t i = 10;
  if (value.size() != 10 + hasTimeOffset * 2 + hasMeasurement * 3 + hasAnnunciation * 2) {
    logger.ERROR("Malformed glucose measurement");
    return;
  }
  if (hasTimeOffset) {
    offsetMinutes = (((int16_t)value[i + 1]) << 8) | value[i];
    i += 2;
  }
  if (hasMeasurement) {
    uint16_t sfloat = (((uint16_t)value[i + 1]) << 8) | value[i];
    sfloat2float(sfloat, &glucose, &precision);
    if (unitMilliMolePerL) {
      // Convert from mol/L to mmol/L (x1000)
      glucose *= 1000.0f;
      precision -= 3;
    } else { // mg/dL
      // Convert from kg/L to mg/dL (x100000)
      glucose *= 100000.0f;
      precision -= 5;
    }
    if (precision < 0)
      precision = 0;
    // cppcheck-suppress unreadVariable
    type = value[2] & 0x0f; // NOLINT
    // cppcheck-suppress unreadVariable
    location = value[2] >> 4; // NOLINT
    i += 3;
  }
  if (hasAnnunciation) {
    // cppcheck-suppress unreadVariable
    annunciation = (((uint16_t)value[i + 1]) << 8) | value[i]; // NOLINT
  }

  string isoTime = convertBOTimeToIsoString(year, month, day, hour, minute, second, offsetMinutes);

  bool filterFirst = context->nextSequenceNumber == 0xFFFF;
  context->nextSequenceNumber = sequenceNo + 1;
  if (filterFirst)
    return;

  if (context->freshMeasurement) {
    // Need to save the updated sequence number if it was fresh
    enqueueWriteConfiguration("phd_transcoding_glucose_sequence_numbers",
                              [context](map<string, string> &configuration) -> void {
                                configuration[context->deviceId] = std::to_string(context->nextSequenceNumber);
                              });
  }

  if (!unitMilliMolePerL) {
    logger.ERROR("Received unit mg/dL, which is currently not supported");
    return;
  }

  Numeric observation;
  observation.handle = 1;
  observation.type = 29112;
  observation.dateAndTime = isoTime;
  observation.unitCode = 4722;
  observation.value = glucose;
  observation.precision = precision;

  set<DIMEntity *> report;
  report.insert(&observation);
  context->observationReceivedCallback(report);
}

void GlucoseService::handleGlucoseContextMeasurement(__attribute__((unused)) vector<uint8_t> value,
                                                     __attribute__((unused)) receiveNewObservationsContext_t context) {}

void GlucoseService::handleRACPResponse(vector<uint8_t> value, receiveNewObservationsContext_t context) {
  if (value.size() != 4 || !(value[0] == 5 || value[0] == 6) || value[1] != 0) {
    logger.ERROR("Malformed RACP response.");
    return;
  }
  if (value[0] == 5) {
    // Number of stored records.
    // Currently unused...
    // uint16_t recordCount = (((uint16_t)value[3]) << 8) | value[2];
  } else {
    // Response code
    uint8_t opcode = value[2];
    uint8_t response = value[3];
    if (opcode == 1) {
      if (response == 1) { // SUCCESS
        logger.DEBUG("Reading stored records complete");
        // Future measurements will be fresh and will require a new sequence number save
        context->freshMeasurement = true;
        // If this was the first time the device was used and it contained no records: next time start from 0
        if (context->nextSequenceNumber == 0xFFFF)
          context->nextSequenceNumber = 0;
        // Save the next sequence number
        enqueueWriteConfiguration("phd_transcoding_glucose_sequence_numbers",
                                  [context](map<string, string> &configuration) -> void {
                                    configuration[context->deviceId] = std::to_string(context->nextSequenceNumber);
                                  });

      } else if (response == 6) { // No records found
        logger.DEBUG("Reading stored records complete - no records found");
        // No need to save the next sequence number as it is unchanged
      } else { // Some error
        logger.ERROR("Error while reading stored records: " + std::to_string(response));
      }
    }
    // other opcodes are currently not used...
  }
}

void GlucoseService::identifyCharacteristics(list<shared_ptr<GATTCharacteristicProxy>> characteristics,
                                             receiveNewObservationsContext_t context) {
  for (auto characteristic : characteristics) {
    if (characteristic->type == GLUCOSE_MEASUREMENT_CHARACTERISTIC) {
      context->glucoseMeasurement = characteristic;
    } else if (characteristic->type == GLUCOSE_MEASUREMENT_CONTEXT_CHARACTERISTIC) {
      context->glucoseMeasurementContext = characteristic;
    } else if (characteristic->type == GLUCOSE_FEATURE_CHARACTERISTIC) {
      context->glucoseFeature = characteristic;
    } else if (characteristic->type == RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC) {
      context->recordAccessControlPoint = characteristic;
    }
  }
}

void GlucoseService::enqueueSyncTask(function<void()> task) {
  enqueueTask([this, task]() -> void {
    task();
    taskQueueDispatch();
  });
}

void GlucoseService::enqueueCharacteristicReadValue(shared_ptr<GATTCharacteristicProxy> characteristic,
                                                    function<void(vector<uint8_t>)> consumer) {
  enqueueTask([this, characteristic, consumer]() -> void {
    characteristic->readValue(make_shared<Response<vector<uint8_t>>>(
        [this, consumer](__attribute__((unused)) bool more, vector<uint8_t> value) -> void {
          consumer(value);
          taskQueueDispatch();
        },
        [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
          logger.ERROR("Failed to read characteristic: " + e->message);
        }));
  });
}

void GlucoseService::enqueueCharacteristicWriteValue(shared_ptr<GATTCharacteristicProxy> characteristic,
                                                     function<vector<uint8_t>()> producer) {
  enqueueTask([this, characteristic, producer]() -> void {
    characteristic->writeValueWithResponse(
        producer(), make_shared<Response<>>([this](__attribute__((unused)) bool more) -> void { taskQueueDispatch(); },
                                            [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                                              logger.ERROR("Failed to write characteristic: " + e->message);
                                            }));
  });
}

void GlucoseService::enqueueCharacteristicSetNotification(shared_ptr<GATTCharacteristicProxy> characteristic,
                                                          bool enable, function<void(vector<uint8_t>)> consumer) {
  enqueueTask([this, characteristic, enable, consumer]() -> void {
    characteristic->setNotification(
        enable, consumer,
        make_shared<Response<>>([this](__attribute__((unused)) bool more) -> void { taskQueueDispatch(); },
                                [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                                  logger.ERROR("Failed to set notification for characteristic: " + e->message);
                                }));
  });
}

void GlucoseService::enqueueReadConfiguration(string configurationName, function<void(map<string, string>)> consumer) {
  enqueueTask([this, configurationName, consumer]() -> void {
    configSettingsConsumerEndpoint.readConfiguration(
        configStore, configurationName,
        make_shared<Response<map<string, string>>>(
            [this, consumer](__attribute__((unused)) bool more, map<string, string> configuration) -> void {
              consumer(configuration);
              taskQueueDispatch();
            },
            [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
              logger.ERROR("Failed to read configuration: " + e->message);
            }));
  });
}

void GlucoseService::enqueueWriteConfiguration(string configurationName,
                                               function<void(map<string, string> &)> producer) {
  enqueueTask([this, configurationName, producer]() -> void {
    map<string, string> configuration;
    producer(configuration);
    configSettingsConsumerEndpoint.writeConfiguration(
        configStore, configurationName, configuration,
        make_shared<Response<>>([this](__attribute__((unused)) bool more) -> void { taskQueueDispatch(); },
                                [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                                  logger.ERROR("Failed to write configuration: " + e->message);
                                }));
  });
}

void GlucoseService::enqueueTask(function<void()> task) {
  bool startTask = tasks.empty();
  tasks.push_back(move(task));
  if (startTask) {
    tasks.front()();
  }
}

void GlucoseService::taskQueueDispatch() {
  tasks.pop_front();
  if (!tasks.empty()) {
    tasks.front()();
  }
}

string GlucoseService::readString(uint8_t *from, size_t size) { return string(reinterpret_cast<char *>(from), size); }

uint64_t GlucoseService::readUInt64(uint8_t *from, size_t size) {
  if (size != 8) {
    logger.ERROR("Wrong length while attempting to read a uint64 type characteristic");
    return 0;
  }
  uint64_t retval = 0;
  from += 7;
  for (int i = 0; i < 8; i++) {
    retval <<= 8;
    retval |= *(from--);
  }
  return retval;
}

} // namespace phd_transcoding
} // namespace s4
