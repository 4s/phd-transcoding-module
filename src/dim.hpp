
#ifndef DIM_HPP
#define DIM_HPP

#include <list>

namespace s4 {
namespace dim {

struct DIMEntity {
  uint16_t handle;
  // DIM objects are compared by the handle only.
  bool operator<(const DIMEntity &rhs) const { return handle < rhs.handle; }
  virtual ~DIMEntity() = default;
};

struct MDS : DIMEntity {
  struct TypeVer {
    uint16_t type; // always MDC_PART_INFRA partition
    uint16_t version;
  };
  std::list<TypeVer> systemType;
  std::string manufacturer;
  std::string modelNumber;
  uint64_t systemId;
  uint64_t transportId; // Should be a list of type-value pairs instead
  struct ProdSpec {
    enum SpecType : uint16_t {
      unspecified = 0,
      serialNumber = 1,
      partNumber = 2,
      hwRevision = 3,
      swRevision = 4,
      fwRevision = 5,
      protocolRevision = 6,
      prodSpecGMDN = 7
    } specType;
    uint16_t privateOID;
    std::string prodSpec;
  };
  std::list<ProdSpec> productionSpecification;
};

struct Metric : DIMEntity {
  uint32_t type;
  string dateAndTime; // ISO format
};

struct Numeric : Metric {
  uint16_t unitCode;
  float value;
  uint8_t precision; // Number of decimals in value.
};

} // namespace dim
} // namespace s4

#endif // DIM_HPP
