

#include "gatt_consumer.hpp"

#include "GATT.pb.h"

using s4::BasePlate::CoreError;
using s4::BasePlate::EndpointFactoryTemporary;
using s4::BasePlate::MetaData;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using namespace s4::messages::interfaces;
using namespace s4::messages::classes;
using std::function;
using std::list;
using std::make_shared;
using std::map;
using std::move;
using std::shared_ptr;
using std::string;
using std::vector;

namespace s4 {
namespace messages_support {

GATTConsumerEndpoint::GATTConsumerEndpoint(EndpointFactoryTemporary &epFactory) : epFactory(epFactory) {

  epFactory.addFunction<gatt::P2C>(
      "GATT.P2C", [this](__attribute__((unused)) MetaData metadata, gatt::P2C *p2c) -> void {
        if (p2c->has_event()) {
          auto event = p2c->event();
          if (event.has_disconnected()) {
            gatt::Disconnected disconn = event.disconnected();
            if (gattServers.count(disconn.gatt_handle())) {
              gattServers[disconn.gatt_handle()]->disconnectedCallback(
                  gattServers[disconn.gatt_handle()], static_cast<GATTDisconnectedReason>(disconn.reason()));
            }
          } else if (event.has_notification()) {
            gatt::Notification noti = event.notification();
            if (notificationHandlers.count(noti.characteristic())) {
              notificationHandlers[noti.characteristic()](vector<uint8_t>(noti.value().begin(), noti.value().end()));
            }
          }
        }
      });
}

shared_ptr<GATTServerProxy> GATTConsumerEndpoint::createServerProxy(
    peer_t handler, objId_t handle,
    function<void(shared_ptr<GATTServerProxy>, GATTDisconnectedReason)> disconnectedCallback) {
  if (gattServers.count(handle)) {
    // Already exist. Return previously created object
    return gattServers[handle];
  }
  auto retval = shared_ptr<GATTServerProxy>(new GATTServerProxy(*this, handler, handle, disconnectedCallback));
  gattServers[handle] = retval;
  return retval;
}

void GATTConsumerEndpoint::connect(peer_t handler, objId_t handle, shared_ptr<Response<>> response) {
  auto connect = new gatt::Connect();
  connect->set_gatt_handle(handle);
  auto request = new gatt::C2P::Request();
  request->set_allocated_connect(connect);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::ConnectSuccess>(
      handler, "GATT.C2P",
      [response](MetaData metadata, __attribute__((unused)) gatt::ConnectSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE");
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::disconnect(peer_t handler, objId_t handle) {
  auto disconnect = new gatt::Disconnect();
  disconnect->set_gatt_handle(handle);
  auto event = new gatt::C2P::Event();
  event->set_allocated_disconnect(disconnect);
  auto msg = gatt::C2P();
  msg.set_allocated_event(event);
  epFactory.getParent().sendUnicast(handler, "GATT.C2P", "N/A", &msg);
}

gatt::BTUUID *to_proto(const BluetoothUUID &uuid) {
  auto retval = new gatt::BTUUID();
  if (uuid.leastSignificant == BluetoothUUID::BT_UUID_LSB &&
      (uuid.mostSignificant & 0x00000000FFFFFFFF) == BluetoothUUID::BT_UUID_MSB) {
    retval->set_short_((uint32_t)(uuid.mostSignificant >> 32));
  } else {
    uint8_t concatenated[16];
    uint64_t accum = uuid.mostSignificant;
    for (int i = 7; i >= 0; i--, accum >>= 8)
      concatenated[i] = (uint8_t)(accum & 0xFF);
    accum = uuid.leastSignificant;
    for (int i = 15; i > 7; i--, accum >>= 8)
      concatenated[i] = (uint8_t)(accum & 0xFF);
    retval->set_long_(concatenated, 16);
  }
  return retval;
}

BluetoothUUID from_proto(const gatt::BTUUID &uuid) {
  switch (uuid.size_case()) {
  case gatt::BTUUID::kShort:
    return BluetoothUUID(uuid.short_());
  case gatt::BTUUID::kLong:
    if (uuid.long_().size() != 16)
      throw "Illegal BTUUID size";
    {
      uint64_t mostSignificant = 0, leastSignificant = 0;
      const char *bytes = uuid.long_().c_str();
      for (int i = 0; i < 8; i++, bytes++) {
        mostSignificant <<= 8;
        mostSignificant |= (*bytes) & 0xFF;
      }
      for (int i = 0; i < 8; i++, bytes++) {
        leastSignificant <<= 8;
        leastSignificant |= (*bytes) & 0xFF;
      }
      return BluetoothUUID(mostSignificant, leastSignificant);
    }
  default:
    throw "Empty BTUUID";
  }
}

void GATTConsumerEndpoint::discoverServices(GATTServerProxy &serverProxy, BluetoothUUID *uuid,
                                            shared_ptr<Response<list<shared_ptr<GATTServiceProxy>>>> response) {
  auto discoverServices = new gatt::DiscoverServices();
  discoverServices->set_gatt_handle(serverProxy.handle);
  if (uuid)
    discoverServices->set_allocated_uuid(to_proto(*uuid));
  auto request = new gatt::C2P::Request();
  request->set_allocated_discover_services(discoverServices);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::DiscoverServicesSuccess>(
      serverProxy.handler, "GATT.C2P",
      [&serverProxy, this, response](MetaData metadata, gatt::DiscoverServicesSuccess *resp) -> void {
        list<shared_ptr<GATTServiceProxy>> target;
        auto source = resp->services();
        for (auto service : source) {
          objId_t handle = service.handle();
          shared_ptr<GATTServiceProxy> proxy;
          if (serverProxy.services.count(handle)) {
            // Known service. Add the existing proxy.
            proxy = serverProxy.services[handle];
          } else {
            // First time we encounter this. Create a new proxy object representing this service
            proxy = shared_ptr<GATTServiceProxy>(new GATTServiceProxy(*this, serverProxy, service.handle(),
                                                                      from_proto(service.uuid()), service.primary()));
            serverProxy.services[handle] = proxy;
          }
          target.push_back(move(proxy));
        }
        response->success(metadata.signal == "OK.MORE", target);
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::findIncludedServices(GATTServiceProxy &serviceProxy,
                                                shared_ptr<Response<list<shared_ptr<GATTServiceProxy>>>> response) {
  auto findIncludedServices = new gatt::FindIncludedServices();
  findIncludedServices->set_service(serviceProxy.handle);
  auto request = new gatt::C2P::Request();
  request->set_allocated_find_included_services(findIncludedServices);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::FindIncludedServicesSuccess>(
      serviceProxy.server.handler, "GATT.C2P",
      [&serviceProxy, this, response](MetaData metadata, gatt::FindIncludedServicesSuccess *resp) -> void {
        list<shared_ptr<GATTServiceProxy>> target;
        auto source = resp->services();
        for (auto service : source) {
          objId_t handle = service.handle();
          shared_ptr<GATTServiceProxy> proxy;
          if (serviceProxy.includedServices.count(handle)) {
            // Known service. Add the existing proxy.
            proxy = serviceProxy.includedServices[handle];
          } else {
            // First time we encounter this. Create a new proxy object representing this service
            proxy = shared_ptr<GATTServiceProxy>(new GATTServiceProxy(*this, serviceProxy.server, service.handle(),
                                                                      from_proto(service.uuid()), service.primary()));
            serviceProxy.includedServices[handle] = proxy;
          }
          target.push_back(move(proxy));
        }
        response->success(metadata.signal == "OK.MORE", target);
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::discoverCharacteristics(
    GATTServiceProxy &serviceProxy, BluetoothUUID *uuid,
    shared_ptr<Response<list<shared_ptr<GATTCharacteristicProxy>>>> response) {
  auto discoverCharacteristics = new gatt::DiscoverCharacteristics();
  discoverCharacteristics->set_service(serviceProxy.handle);
  if (uuid)
    discoverCharacteristics->set_allocated_uuid(to_proto(*uuid));
  auto request = new gatt::C2P::Request();
  request->set_allocated_discover_characteristics(discoverCharacteristics);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::DiscoverCharacteristicsSuccess>(
      serviceProxy.server.handler, "GATT.C2P",
      [&serviceProxy, this, response](MetaData metadata, gatt::DiscoverCharacteristicsSuccess *resp) -> void {
        list<shared_ptr<GATTCharacteristicProxy>> target;
        auto source = resp->characteristics();
        for (auto characteristic : source) {
          objId_t handle = characteristic.handle();
          shared_ptr<GATTCharacteristicProxy> proxy;
          if (serviceProxy.server.characteristics.count(handle)) {
            // Known characteristic. Add the existing proxy.
            proxy = serviceProxy.server.characteristics[handle];
          } else {
            // First time we encounter this. Create a new proxy object representing this characteristic
            proxy = shared_ptr<GATTCharacteristicProxy>(new GATTCharacteristicProxy(
                *this, serviceProxy.server, characteristic.handle(), from_proto(characteristic.uuid()),
                characteristic.broadcast(), characteristic.read(), characteristic.write_without_response(),
                characteristic.write(), characteristic.notify(), characteristic.indicate(),
                characteristic.authenticated_signed_write(), characteristic.extended_properties()));
            serviceProxy.server.characteristics[handle] = proxy;
          }
          target.push_back(move(proxy));
        }
        response->success(metadata.signal == "OK.MORE", target);
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::discoverDescriptors(GATTCharacteristicProxy &characteristicProxy,
                                               shared_ptr<Response<list<shared_ptr<GATTDescriptorProxy>>>> response) {
  auto discoverDescriptors = new gatt::DiscoverDescriptors();
  discoverDescriptors->set_characteristic(characteristicProxy.handle);
  auto request = new gatt::C2P::Request();
  request->set_allocated_discover_descriptors(discoverDescriptors);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::DiscoverDescriptorsSuccess>(
      characteristicProxy.server.handler, "GATT.C2P",
      [&characteristicProxy, response](MetaData metadata, gatt::DiscoverDescriptorsSuccess *resp) -> void {
        list<shared_ptr<GATTDescriptorProxy>> target;
        auto source = resp->descriptors();
        for (auto descriptor : source) {
          objId_t handle = descriptor.handle();
          shared_ptr<GATTDescriptorProxy> proxy;
          if (characteristicProxy.server.descriptors.count(handle)) {
            // Known descriptor. Add the existing proxy.
            proxy = characteristicProxy.server.descriptors[handle];
          } else {
            // First time we encounter this. Create a new proxy object representing this characteristic
            proxy = shared_ptr<GATTDescriptorProxy>
                // (new GATTDescriptorProxy(*this, characteristicProxy.server, descriptor.handle(), descriptor.type()));
                (new string("Descriptor type: " + std::to_string(descriptor.type())));
            characteristicProxy.server.descriptors[handle] = proxy;
          }
          target.push_back(move(proxy));
        }
        response->success(metadata.signal == "OK.MORE", target);
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::readValue(GATTCharacteristicProxy &characteristicProxy,
                                     shared_ptr<Response<vector<uint8_t>>> response) {
  auto readCharacteristic = new gatt::ReadCharacteristic();
  readCharacteristic->set_characteristic(characteristicProxy.handle);
  auto request = new gatt::C2P::Request();
  request->set_allocated_read_characteristic(readCharacteristic);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::ReadCharacteristicSuccess>(
      characteristicProxy.server.handler, "GATT.C2P",
      [response](MetaData metadata, gatt::ReadCharacteristicSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE", vector<uint8_t>(resp->value().begin(), resp->value().end()));
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::writeValueWithResponse(GATTCharacteristicProxy &characteristicProxy, vector<uint8_t> value,
                                                  shared_ptr<Response<>> response) {
  auto writeCharacteristic = new gatt::WriteCharacteristic();
  writeCharacteristic->set_characteristic(characteristicProxy.handle);
  writeCharacteristic->set_value(value.data(), value.size());
  auto request = new gatt::C2P::Request();
  request->set_allocated_write_characteristic(writeCharacteristic);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::WriteCharacteristicSuccess>(
      characteristicProxy.server.handler, "GATT.C2P",
      [response](MetaData metadata, __attribute__((unused)) gatt::WriteCharacteristicSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE");
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

void GATTConsumerEndpoint::writeValueWithoutResponse(GATTCharacteristicProxy &characteristicProxy,
                                                     vector<uint8_t> value, shared_ptr<Response<>> response) {
  auto writeCharacteristic = new gatt::WriteCharacteristicWithoutResponse();
  writeCharacteristic->set_characteristic(characteristicProxy.handle);
  writeCharacteristic->set_value(value.data(), value.size());
  auto request = new gatt::C2P::Request();
  request->set_allocated_write_characteristic_without_response(writeCharacteristic);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);
  epFactory.sendUnicastWithCallback<gatt::WriteCharacteristicWithoutResponseSuccess>(
      characteristicProxy.server.handler, "GATT.C2P",
      [response](MetaData metadata, __attribute__((unused)) gatt::WriteCharacteristicWithoutResponseSuccess *resp)
          -> void { response->success(metadata.signal == "OK.MORE"); },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

//    void PHDTranscodingModule::PImpl::readDescriptor(peer_t peer, objId_t descriptor) {
//      auto readDescriptor = new gatt::ReadDescriptor();
//      readDescriptor->set_descr(descriptor);
//      auto request = new gatt::C2P::Request();
//      request->set_allocated_read_descriptor(readDescriptor);
//      auto msg = gatt::C2P();
//      msg.set_allocated_request(request);
//      parent.sendUnicastWithCallback(peer, "GATT.C2P", "N/A", "0008", &msg);
//    }

//      addFunction<gatt::ReadDescriptorSuccess>
//        ("#0008_OK.",
//         [this](BasePlate::MetaData metadata, gatt::ReadDescriptorSuccess* payload) -> void {
//          pImpl->readDescriptorSuccess();
//        });

//    void PHDTranscodingModule::PImpl::writeDescriptor(peer_t peer, objId_t descriptor, vector<uint8_t> value) {
//      auto writeDescriptor = new gatt::WriteDescriptor();
//      writeDescriptor->set_descr(descriptor);
// //FIXME value
//      auto request = new gatt::C2P::Request();
//      request->set_allocated_write_descriptor(writeDescriptor);
//      auto msg = gatt::C2P();
//      msg.set_allocated_request(request);
//      parent.sendUnicastWithCallback(peer, "GATT.C2P", "N/A", "0009", &msg);
//    }

//      addFunction<gatt::WriteDescriptorSuccess>
//        ("#0009_OK.",
//         [this](BasePlate::MetaData metadata, gatt::WriteDescriptorSuccess* payload) -> void {
//          pImpl->writeDescriptorSuccess();
//        });

void GATTConsumerEndpoint::setNotification(GATTCharacteristicProxy &characteristicProxy, bool enable,
                                           function<void(vector<uint8_t>)> notificationCallback,
                                           shared_ptr<Response<>> response) {
  auto setNotification = new gatt::SetNotification();
  setNotification->set_characteristic(characteristicProxy.handle);
  setNotification->set_enable_notification(enable);
  auto request = new gatt::C2P::Request();
  request->set_allocated_set_notification(setNotification);
  auto msg = gatt::C2P();
  msg.set_allocated_request(request);

  // Set or remove the callback
  if (enable) {
    notificationHandlers[characteristicProxy.handle] = notificationCallback;
  } else {
    notificationHandlers.erase(characteristicProxy.handle);
  }
  epFactory.sendUnicastWithCallback<gatt::SetNotificationSuccess>(
      characteristicProxy.server.handler, "GATT.C2P",
      [response](MetaData metadata, __attribute__((unused)) gatt::SetNotificationSuccess *resp) -> void {
        response->success(metadata.signal == "OK.MORE");
      },
      [response](MetaData metadata, shared_ptr<CoreError> err) -> void {
        response->error(metadata.signal == "ERR.MORE", err);
      },
      &msg);
}

GATTServerProxy::GATTServerProxy(
    GATTConsumerEndpoint &endpoint, peer_t handler, objId_t handle,
    function<void(shared_ptr<GATTServerProxy>, GATTDisconnectedReason)> disconnectedCallback)
    : endpoint(endpoint), handler(handler), handle(handle), disconnectedCallback(disconnectedCallback) {}

void GATTServerProxy::connect(shared_ptr<Response<>> response) { endpoint.connect(handler, handle, response); }

void GATTServerProxy::disconnect() { endpoint.disconnect(handler, handle); }

void GATTServerProxy::discoverServices(BluetoothUUID *uuid,
                                       shared_ptr<Response<list<shared_ptr<GATTServiceProxy>>>> response) {
  endpoint.discoverServices(*this, uuid, response);
}

GATTServiceProxy::GATTServiceProxy(GATTConsumerEndpoint &endpoint, GATTServerProxy &server, objId_t handle,
                                   BluetoothUUID type, bool primary)
    : type(type), primary(primary), endpoint(endpoint), server(server), handle(handle) {}

void GATTServiceProxy::findIncludedServices(shared_ptr<Response<list<shared_ptr<GATTServiceProxy>>>> response) {
  endpoint.findIncludedServices(*this, response);
}

void GATTServiceProxy::discoverCharacteristics(
    BluetoothUUID *uuid, shared_ptr<Response<list<shared_ptr<GATTCharacteristicProxy>>>> response) {
  endpoint.discoverCharacteristics(*this, uuid, response);
}

GATTCharacteristicProxy::GATTCharacteristicProxy(GATTConsumerEndpoint &endpoint, GATTServerProxy &server,
                                                 objId_t handle, BluetoothUUID type, bool canBroadcast, bool canRead,
                                                 bool canWriteWithoutResponse, bool canWriteWithResponse,
                                                 bool canNotify, bool canIndicate, bool canAuthenticatedSignedWrite,
                                                 bool hasExtendedProperties)
    : type(type), canBroadcast(canBroadcast), canRead(canRead), canWriteWithoutResponse(canWriteWithoutResponse),
      canWriteWithResponse(canWriteWithResponse), canNotify(canNotify), canIndicate(canIndicate),
      canAuthenticatedSignedWrite(canAuthenticatedSignedWrite), hasExtendedProperties(hasExtendedProperties),
      endpoint(endpoint), server(server), handle(handle) {}

void GATTCharacteristicProxy::discoverDescriptors(
    shared_ptr<Response<list<shared_ptr<GATTDescriptorProxy>>>> response) {
  endpoint.discoverDescriptors(*this, response);
}

void GATTCharacteristicProxy::readValue(shared_ptr<Response<vector<uint8_t>>> response) {
  endpoint.readValue(*this, response);
}

void GATTCharacteristicProxy::writeValueWithResponse(vector<uint8_t> value, shared_ptr<Response<>> response) {
  endpoint.writeValueWithResponse(*this, value, response);
}

void GATTCharacteristicProxy::writeValueWithoutResponse(vector<uint8_t> value, shared_ptr<Response<>> response) {
  endpoint.writeValueWithoutResponse(*this, value, response);
}

void GATTCharacteristicProxy::setNotification(bool enable, function<void(vector<uint8_t>)> notificationCallback,
                                              shared_ptr<Response<>> response) {
  endpoint.setNotification(*this, enable, notificationCallback, response);
}

} // namespace messages_support
} // namespace s4
