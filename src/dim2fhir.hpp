
#ifndef DIM2FHIR_HPP
#define DIM2FHIR_HPP

#include "BasePlate-fix.hpp"
#include "dim.hpp"
#include "fhir_measurement_producer.hpp"

#include <set>

namespace s4 {
namespace dim2fhir {

class DIM2FHIRConverter {
public:
  /**
   * @param fhirEndpoint Reference to the FHIR producer endpoint.
   *                     Ownership is transferred to the DIM2FHIRConverter
   *                     instance.
   * @param logger       Reference to the logger object which must be valid
   *                     for the entire lifetime of the DIM2FHIRConverter
   *                     instance
   */
  DIM2FHIRConverter(s4::messages_support::FHIRMeasurementProducerEndpoint &fhirEndpoint, s4::BasePlate::Logger &logger);

  void convertScanReport(const s4::dim::MDS &mdsObject, const std::set<s4::dim::DIMEntity *> &reportedObjects);

private:
  s4::messages_support::FHIRMeasurementProducerEndpoint &fhirEndpoint;
  __attribute__((unused)) s4::BasePlate::Logger &logger;
};

} // namespace dim2fhir
} // namespace s4

#endif // DIM2FHIR_HPP
