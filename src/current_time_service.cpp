

#include "current_time_service.hpp"

using s4::BasePlate::Logger;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::messages_support::ConfigSettingsConsumer;
using s4::messages_support::ConfigSettingsConsumerEndpoint;
using s4::messages_support::TimeConsumer;
using s4::messages_support::TimeConsumerEndpoint;
using std::vector;

namespace s4 {
namespace phd_transcoding {

class CurrentTimeService::EventHandler : public ConfigSettingsConsumer, public TimeConsumer {
  friend class CurrentTimeService;

private:
  CurrentTimeService *cts;
  explicit EventHandler(CurrentTimeService *cts) : cts(cts) {}
  void kill() { cts = nullptr; }

public:
  void announceStore(const peer_t &sender) override {
    if (cts)
      cts->announceStore(sender);
  }
  void announceClock(const peer_t &sender) override {
    if (cts)
      cts->announceClock(sender);
  }
};

CurrentTimeService::CurrentTimeService(ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                                       TimeConsumerEndpoint &timeConsumerEndpoint, Logger &logger)
    : configSettingsConsumerEndpoint(configSettingsConsumerEndpoint), timeConsumerEndpoint(timeConsumerEndpoint),
      logger(logger), eventHandler(new EventHandler(this)) {
  configSettingsConsumerEndpoint.addConsumer(eventHandler);
  timeConsumerEndpoint.addConsumer(eventHandler);
}

CurrentTimeService::~CurrentTimeService() { eventHandler->kill(); }

void CurrentTimeService::announceStore(__attribute__((unused)) const peer_t &sender) {}

void CurrentTimeService::announceClock(__attribute__((unused)) const peer_t &sender) {}

} // namespace phd_transcoding
} // namespace s4
