
#ifndef GLUCOSE_SERVICE_HPP
#define GLUCOSE_SERVICE_HPP

#include "BasePlate-fix.hpp"
#include "dim.hpp"

#include "config_settings_consumer.hpp"
#include "gatt_consumer.hpp"
#include "time_consumer.hpp"

#include <memory>

namespace s4 {
namespace phd_transcoding {

class GlucoseService {

public:
  GlucoseService(s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                 s4::messages_support::TimeConsumerEndpoint &timeConsumerEndpoint, s4::BasePlate::Logger &logger);
  ~GlucoseService();

  void receiveNewObservations(std::shared_ptr<s4::messages_support::GATTServiceProxy> glucoseServiceProxy,
                              std::string deviceId,
                              std::function<void(std::set<s4::dim::DIMEntity *> &)> observationReceivedCallback);

private:
  s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint;
  s4::BasePlate::Logger &logger;

  class EventHandler;
  std::shared_ptr<EventHandler> eventHandler;

  void announceStore(const s4::BasePlate::peer_t &sender);
  void announceClock(const s4::BasePlate::peer_t &sender);

  s4::BasePlate::peer_t clock, configStore;

  struct ReceiveNewObservationsContext {
    std::shared_ptr<s4::dim::Numeric> glucoseObject;
    uint16_t featureFlags;
    uint16_t nextSequenceNumber;
    bool freshMeasurement;
    std::string deviceId;
    std::shared_ptr<s4::messages_support::GATTServiceProxy> glucoseServiceProxy;
    std::function<void(std::set<s4::dim::DIMEntity *> &)> observationReceivedCallback;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> glucoseMeasurement;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> glucoseMeasurementContext;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> glucoseFeature;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> recordAccessControlPoint;
  };
  typedef std::shared_ptr<ReceiveNewObservationsContext> receiveNewObservationsContext_t;

  void
  identifyCharacteristics(std::list<std::shared_ptr<s4::messages_support::GATTCharacteristicProxy>> characteristics,
                          receiveNewObservationsContext_t context);
  void handleGlucoseMeasurement(std::vector<uint8_t> value, receiveNewObservationsContext_t context);
  void handleGlucoseContextMeasurement(std::vector<uint8_t> value, receiveNewObservationsContext_t context);
  void handleRACPResponse(std::vector<uint8_t> value, receiveNewObservationsContext_t context);

  std::list<std::function<void()>> tasks;

  void enqueueCharacteristicReadValue(std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> characteristic,
                                      std::function<void(std::vector<uint8_t>)> consumer);
  void enqueueCharacteristicWriteValue(std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> characteristic,
                                       std::function<std::vector<uint8_t>()> producer);
  void
  enqueueCharacteristicSetNotification(std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> characteristic,
                                       bool enable, std::function<void(std::vector<uint8_t>)> consumer);
  void enqueueReadConfiguration(std::string configurationName,
                                std::function<void(std::map<std::string, std::string>)> consumer);
  void enqueueWriteConfiguration(std::string configurationName,
                                 std::function<void(std::map<std::string, std::string> &)> producer);
  void enqueueSyncTask(std::function<void()> task);
  void enqueueTask(std::function<void()> task);
  void taskQueueDispatch();

  std::string readString(uint8_t *from, size_t size);
  uint64_t readUInt64(uint8_t *from, size_t size);
};

} // namespace phd_transcoding
} // namespace s4

#endif // GLUCOSE_SERVICE_HPP
