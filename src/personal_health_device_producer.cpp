

#include "personal_health_device_producer.hpp"

#include "PersonalHealthDevice.pb.h"

using namespace s4::messages::interfaces;
using s4::BasePlate::EndpointFactoryTemporary;
using std::shared_ptr;

namespace s4 {
namespace messages_support {

PersonalHealthDeviceProducerEndpoint::PersonalHealthDeviceProducerEndpoint(EndpointFactoryTemporary &epFactory) {
  epFactory.addFunction<personal_health_device::C2P>(
      "PersonalHealthDevice.C2P", [this](BasePlate::MetaData metadata, personal_health_device::C2P *c2p) -> void {
        if (c2p->has_event()) {
          auto event = c2p->event();
          if (event.has_subscribe_device_type()) {
            personal_health_device::SubscribeDeviceType sdt = event.subscribe_device_type();
            int mdcCode = sdt.mdc_code();
            for (auto producer : producers)
              producer->subscribeDeviceType(metadata.sender, mdcCode);
          } else if (event.has_unsubscribe_device_type()) {
            personal_health_device::UnsubscribeDeviceType udt = event.unsubscribe_device_type();
            int mdcCode = udt.mdc_code();
            for (auto producer : producers)
              producer->unsubscribeDeviceType(metadata.sender, mdcCode);
          }
        }
      });
}

void PersonalHealthDeviceProducerEndpoint::addProducer(shared_ptr<PersonalHealthDeviceProducer> producer) {
  producers.insert(producer);
}

} // namespace messages_support
} // namespace s4
