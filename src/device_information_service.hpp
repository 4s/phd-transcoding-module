
#ifndef DEVICE_INFORMATION_SERVICE_HPP
#define DEVICE_INFORMATION_SERVICE_HPP

#include "BasePlate-fix.hpp"
#include "dim.hpp"

#include "config_settings_consumer.hpp"
#include "gatt_consumer.hpp"

#include <functional>
#include <memory>

namespace s4 {
namespace phd_transcoding {

class DeviceInformationService {

public:
  DeviceInformationService(s4::messages_support::ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                           s4::BasePlate::Logger &logger);
  ~DeviceInformationService();

  void populateMDS(std::shared_ptr<s4::dim::MDS> mdsObject,
                   std::shared_ptr<s4::messages_support::GATTServiceProxy> deviceInformationServiceProxy,
                   std::shared_ptr<s4::messages_support::GATTServiceProxy> genericAccessServiceProxy,
                   std::function<void()> doneCallback);

private:
  s4::BasePlate::Logger &logger;

  class EventHandler;
  std::shared_ptr<EventHandler> eventHandler;

  void announceStore(const s4::BasePlate::peer_t &sender);
  //      void announceClock(const s4::BasePlate::peer_t &sender);

  struct PopulateMDSContext {
    std::shared_ptr<s4::dim::MDS> mdsObject;
    std::shared_ptr<s4::messages_support::GATTServiceProxy> deviceInformationServiceProxy;
    std::shared_ptr<s4::messages_support::GATTServiceProxy> genericAccessServiceProxy;
    std::function<void()> doneCallback;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> manufacturer;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> modelNumber;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> systemId;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> bt_address;
    std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> serialNo;
  };
  typedef std::shared_ptr<PopulateMDSContext> populateMDSContext_t;

  void
  identifyCharacteristics(std::list<std::shared_ptr<s4::messages_support::GATTCharacteristicProxy>> characteristics,
                          populateMDSContext_t context);

  std::list<std::function<void()>> tasks;

  void enqueueCharacteristicReadValue(std::shared_ptr<s4::messages_support::GATTCharacteristicProxy> characteristic,
                                      std::function<void(std::vector<uint8_t>)> consumer);
  void enqueueSyncTask(std::function<void()> task);
  void enqueueTask(std::function<void()> task);
  void taskQueueDispatch();

  std::string readString(uint8_t *from, size_t size);
  uint64_t readUInt64(uint8_t *from, size_t size);
};

} // namespace phd_transcoding
} // namespace s4

#endif // DEVICE_INFORMATION_SERVICE_HPP
