
#ifndef TIME_CONSUMER_HPP
#define TIME_CONSUMER_HPP

#include "BasePlate-fix.hpp"

#include <memory>
#include <set>

namespace s4 {
namespace messages_support {

class TimeConsumer {
public:
  virtual void announceClock(const s4::BasePlate::peer_t &sender) = 0;
  virtual ~TimeConsumer() = default;
};

class TimeConsumerEndpoint {

public:
  explicit TimeConsumerEndpoint(s4::BasePlate::EndpointFactoryTemporary &epFactory);
  void addConsumer(std::shared_ptr<TimeConsumer> consumer);

  struct TimeParts {
    int32_t year;
    int32_t month;    // 1-12
    int32_t day;      // 1-31
    int32_t hour;     // 0-23
    int32_t min;      // 0-59
    int32_t sec;      // 0-60 (note: clock MAY - or might not(!) - report leap seconds)
    int32_t ms;       // 0-999
    int32_t timezone; // -720 - 840 (UTC offset in minutes)
  };
  void solicitClock();
  enum format_t : int { PARTS = 0, ISO8601EXT = 1 };
  void readTime(s4::BasePlate::peer_t peer, bool utc, TimeConsumerEndpoint::format_t format,
                std::shared_ptr<s4::BasePlate::Response<bool, TimeParts, string>> response);
  void startTimer(s4::BasePlate::peer_t peer, uint32_t delayMillis, bool repeat,
                  std::shared_ptr<s4::BasePlate::Response<int32_t, bool>> response);
  void stopTimer(s4::BasePlate::peer_t peer, int32_t timerId, std::shared_ptr<s4::BasePlate::Response<>> response);

private:
  s4::BasePlate::EndpointFactoryTemporary &epFactory;
  std::set<std::shared_ptr<TimeConsumer>> consumers;
};

} // namespace messages_support
} // namespace s4

#endif // TIME_CONSUMER_HPP
