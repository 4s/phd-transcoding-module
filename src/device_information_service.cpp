

#include "device_information_service.hpp"

using s4::BasePlate::CoreError;
using s4::BasePlate::Logger;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using s4::dim::MDS;
using namespace s4::messages_support::BluetoothUUIDs;
using s4::messages_support::ConfigSettingsConsumer;
using s4::messages_support::ConfigSettingsConsumerEndpoint;
using s4::messages_support::GATTCharacteristicProxy;
using s4::messages_support::GATTServiceProxy;
using std::function;
using std::list;
using std::make_shared;
using std::move;
using std::shared_ptr;
using std::vector;

namespace s4 {
namespace phd_transcoding {

class DeviceInformationService::EventHandler : public ConfigSettingsConsumer {
  friend class DeviceInformationService;

private:
  DeviceInformationService *dis;
  explicit EventHandler(DeviceInformationService *dis) : dis(dis) {}
  void kill() { dis = nullptr; }

public:
  void announceStore(const peer_t &sender) override {
    if (dis)
      dis->announceStore(sender);
  }
};

DeviceInformationService::DeviceInformationService(ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                                                   Logger &logger)
    : logger(logger), eventHandler(new EventHandler(this)) {
  configSettingsConsumerEndpoint.addConsumer(eventHandler);
}

DeviceInformationService::~DeviceInformationService() { eventHandler->kill(); }

void DeviceInformationService::announceStore(__attribute__((unused)) const peer_t &sender) {}

void DeviceInformationService::populateMDS(shared_ptr<MDS> mdsObject,
                                           shared_ptr<GATTServiceProxy> deviceInformationServiceProxy,
                                           shared_ptr<GATTServiceProxy> genericAccessServiceProxy,
                                           function<void()> doneCallback) {
  // The MDS handle is fixed to 0
  mdsObject->handle = 0;

  // Create a context representing this progress/state of the async function call
  populateMDSContext_t context = make_shared<PopulateMDSContext>();
  context->mdsObject = move(mdsObject);
  context->deviceInformationServiceProxy = move(deviceInformationServiceProxy);
  context->genericAccessServiceProxy = move(genericAccessServiceProxy);
  context->doneCallback = move(doneCallback);

  // First step is to discover all characteristics

  // Read from Device Information Service
  context->deviceInformationServiceProxy->discoverCharacteristics(
      nullptr,
      make_shared<Response<list<shared_ptr<GATTCharacteristicProxy>>>>(
          [this, context](__attribute__((unused)) bool more,
                          list<shared_ptr<GATTCharacteristicProxy>> characteristics) -> void {
            // Identify and store all relevant characteristics
            identifyCharacteristics(characteristics, context);
            // Read from the Generic Access Service
            context->genericAccessServiceProxy->discoverCharacteristics(
                nullptr,
                make_shared<Response<list<shared_ptr<GATTCharacteristicProxy>>>>(
                    [this, context](__attribute__((unused)) bool more,
                                    list<shared_ptr<GATTCharacteristicProxy>> characteristics) -> void {
                      // Identify and store all relevant characteristics
                      identifyCharacteristics(characteristics, context);

                      // Check that all mandatory characteristics were found
                      if (!(context->manufacturer && context->modelNumber && context->systemId && context->serialNo)) {
                        logger.ERROR("Failed to discover mandatory characteristics");
                        return;
                      }

                      // Prepare to read the characteristics

                      enqueueCharacteristicReadValue(
                          context->manufacturer, [this, context](vector<uint8_t> value) -> void {
                            context->mdsObject->manufacturer = readString(value.data(), value.size());
                          });
                      enqueueCharacteristicReadValue(
                          context->modelNumber, [this, context](vector<uint8_t> value) -> void {
                            context->mdsObject->modelNumber = readString(value.data(), value.size());
                          });
                      enqueueCharacteristicReadValue(context->systemId, [this, context](vector<uint8_t> value) -> void {
                        context->mdsObject->systemId = readUInt64(value.data(), value.size());
                      });
                      enqueueCharacteristicReadValue(context->serialNo, [this, context](vector<uint8_t> value) -> void {
                        context->mdsObject->productionSpecification.emplace_back(dim::MDS::ProdSpec(
                            {MDS::ProdSpec::serialNumber, 0, readString(value.data(), value.size())}));
                      });
                      enqueueSyncTask(context->doneCallback);
                    },
                    [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                      logger.ERROR("Failed to read characteristics of the Generic Access Service: " + e->message);
                    }));
          },
          [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
            logger.ERROR("Failed to read characteristics of the Device Information Service" + e->message);
          }));
}

void DeviceInformationService::identifyCharacteristics(list<shared_ptr<GATTCharacteristicProxy>> characteristics,
                                                       populateMDSContext_t context) {
  for (auto characteristic : characteristics) {
    if (characteristic->type == MANUFACTURER_NAME_STRING_CHARACTERISTIC) {
      context->manufacturer = characteristic;
    } else if (characteristic->type == MODEL_NUMBER_STRING_CHARACTERISTIC) {
      context->modelNumber = characteristic;
    } else if (characteristic->type == SYSTEM_ID_CHARACTERISTIC) {
      context->systemId = characteristic;
    } else if (characteristic->type == SERIAL_NUMBER_STRING_CHARACTERISTIC) {
      context->serialNo = characteristic;
    }
  }
}

void DeviceInformationService::enqueueSyncTask(function<void()> task) {
  enqueueTask([this, task]() -> void {
    task();
    taskQueueDispatch();
  });
}

void DeviceInformationService::enqueueCharacteristicReadValue(shared_ptr<GATTCharacteristicProxy> characteristic,
                                                              function<void(vector<uint8_t>)> consumer) {
  enqueueTask([this, characteristic, consumer]() -> void {
    characteristic->readValue(make_shared<Response<vector<uint8_t>>>(
        [this, consumer](__attribute__((unused)) bool more, vector<uint8_t> value) -> void {
          consumer(value);
          taskQueueDispatch();
        },
        [this](__attribute__((unused)) bool more, __attribute__((unused)) shared_ptr<CoreError> e) -> void {
          logger.ERROR("Failed to read characteristic");
        }));
  });
}

void DeviceInformationService::enqueueTask(function<void()> task) {
  bool startTask = tasks.empty();
  tasks.push_back(move(task));
  if (startTask) {
    tasks.front()();
  }
}

void DeviceInformationService::taskQueueDispatch() {
  tasks.pop_front();
  if (!tasks.empty()) {
    tasks.front()();
  }
}

string DeviceInformationService::readString(uint8_t *from, size_t size) {
  return string(reinterpret_cast<char *>(from), size);
}

uint64_t DeviceInformationService::readUInt64(uint8_t *from, size_t size) {
  if (size != 8) {
    logger.ERROR("Wrong length while attempting to read a uint64 type characteristic");
    return 0;
  }
  uint64_t retval = 0;
  from += 7;
  for (int i = 0; i < 8; i++) {
    retval <<= 8;
    retval |= *(from--);
  }
  return retval;
}

} // namespace phd_transcoding
} // namespace s4
