

#include "controller.hpp"

using s4::BasePlate::CoreError;
using s4::BasePlate::handler2peer;
using s4::BasePlate::Logger;
using s4::BasePlate::objId_t;
using s4::BasePlate::peer_t;
using s4::BasePlate::Response;
using s4::dim2fhir::DIM2FHIRConverter;
using namespace s4::dim;
using s4::messages_support::BluetoothUUID;
using s4::messages_support::ConfigSettingsConsumer;
using s4::messages_support::ConfigSettingsConsumerEndpoint;
using s4::messages_support::DeviceControlConsumer;
using s4::messages_support::DeviceControlConsumerEndpoint;
using s4::messages_support::GATTCharacteristicProxy;
using s4::messages_support::GATTConsumerEndpoint;
using s4::messages_support::GATTDescriptorProxy;
using s4::messages_support::GATTDisconnectedReason;
using s4::messages_support::GATTServerProxy;
using s4::messages_support::GATTServiceProxy;
using s4::messages_support::PersonalHealthDeviceProducer;
using s4::messages_support::PersonalHealthDeviceProducerEndpoint;
using s4::messages_support::TimeConsumer;
using s4::messages_support::TimeConsumerEndpoint;
using namespace s4::messages_support::BluetoothUUIDs;
using s4::messages::classes::device::BluetoothDevice_BluetoothProfile;
using s4::messages::classes::device::DeviceTransport; // FIXME: remove deps
using std::list;
using std::make_shared;
using std::set;
using std::shared_ptr;
using std::string;
using std::vector;

namespace s4 {
namespace phd_transcoding {

class Controller::EventHandler : public PersonalHealthDeviceProducer,
                                 public DeviceControlConsumer,
                                 public ConfigSettingsConsumer,
                                 public TimeConsumer {
  friend class Controller;

private:
  Controller *controller;
  explicit EventHandler(Controller *c) : controller(c) {}
  void kill() { controller = nullptr; }

public:
  void subscribeDeviceType(peer_t sender, int mdcCode) override {
    if (controller)
      controller->subscribeDeviceType(sender, mdcCode);
  }
  void unsubscribeDeviceType(peer_t sender, int mdcCode) override {
    if (controller)
      controller->unsubscribeDeviceType(sender, mdcCode);
  }
  void searchResult(peer_t sender, objId_t search_id, string tpid, DeviceTransport deviceTransport) override {
    if (controller)
      controller->searchResult(sender, search_id, tpid, deviceTransport);
  }
  void announceStore(const peer_t &sender) override {
    if (controller)
      controller->announceStore(sender);
  }
  void announceClock(const peer_t &sender) override {
    if (controller)
      controller->announceClock(sender);
  }
};

Controller::Controller(PersonalHealthDeviceProducerEndpoint &phdProducerEndpoint,
                       DeviceControlConsumerEndpoint &deviceControlConsumerEndpoint,
                       ConfigSettingsConsumerEndpoint &configSettingsConsumerEndpoint,
                       TimeConsumerEndpoint &timeConsumerEndpoint, GATTConsumerEndpoint &gattConsumerEndpoint,
                       DIM2FHIRConverter &dim2fhirConverter, DeviceInformationService &deviceInformationService,
                       CurrentTimeService &currentTimeService, GlucoseService &glucoseService, Logger &logger)
    : deviceControlConsumerEndpoint(deviceControlConsumerEndpoint),
      configSettingsConsumerEndpoint(configSettingsConsumerEndpoint), timeConsumerEndpoint(timeConsumerEndpoint),
      gattConsumerEndpoint(gattConsumerEndpoint), dim2fhirConverter(dim2fhirConverter),
      deviceInformationService(deviceInformationService), currentTimeService(currentTimeService),
      glucoseService(glucoseService), logger(logger), eventHandler(new EventHandler(this)), searchid(0),
      deviceFound(false) {
  phdProducerEndpoint.addProducer(eventHandler);
  deviceControlConsumerEndpoint.addConsumer(eventHandler);
  configSettingsConsumerEndpoint.addConsumer(eventHandler);
  timeConsumerEndpoint.addConsumer(eventHandler);
}

Controller::~Controller() { eventHandler->kill(); }

void Controller::subscribeDeviceType(__attribute__((unused)) peer_t sender, int mdcCode) {
  if (configStore.empty())
    configSettingsConsumerEndpoint.solicitStore();
  if (clock.empty())
    timeConsumerEndpoint.solicitClock();

  logger.DEBUG("Subscribe mdcCode: " + std::to_string(mdcCode));
  if (mdcCode == 528401) {
    if (searchid)
      return; // Already subscribed - ignore second request
    searchid = logger.createObjId();
    deviceControlConsumerEndpoint.search(searchid);
  } else {
    // Currently not supported!!
    logger.ERROR("Subscribe to anything but mdcCode=528401 (glucometer) is NOT supported at the moment!");
  }
}

void Controller::unsubscribeDeviceType(__attribute__((unused)) peer_t sender, __attribute__((unused)) int mdcCode) {
  logger.ERROR("Unsubscribing is not supported at the moment");
}

uint64_t bdAddrString2long(string bdAddr) {
  uint64_t retval = 0;
  for (int i = 0; i < 18; i += 3) {
    retval <<= 8;
    retval |= std::stoi(bdAddr.substr(i, 2), nullptr, 16) & 0xFF;
  }
  return retval;
}

void Controller::searchResult(__attribute__((unused)) peer_t sender, __attribute__((unused)) objId_t search_id,
                              __attribute__((unused)) string tpid, DeviceTransport deviceTransport) {
  if (deviceFound)
    return; // Ignore all devices except the first match!
  if (deviceTransport.has_bluetooth_device()) {
    auto btDev = deviceTransport.bluetooth_device();
    if (btDev.name().find("Nonin") == 0 || btDev.name().find("Contour") == 0) { // FIXME: Remove Nonin
      for (auto btProfile : btDev.bluetooth_profiles()) {

        if (btProfile.profile() == BluetoothDevice_BluetoothProfile::GENERIC_ATTRIBUTE_PROFILE) {

          deviceControlConsumerEndpoint.stopSearch(searchid);

          logger.DEBUG("CONNECTING to " + btDev.name() + ", BD_ADDR: " + btDev.bd_addr());

          deviceFound = true;
          uint64_t transportAddress = bdAddrString2long(btDev.bd_addr());

          shared_ptr<shared_ptr<GATTServerProxy>> serverProxyPtr = make_shared<shared_ptr<GATTServerProxy>>();
          auto healthyConnection = make_shared<bool>(false);
          auto connectCallback = make_shared<Response<>>(
              [this, serverProxyPtr, healthyConnection, transportAddress](__attribute__((unused)) bool more) -> void {
                *healthyConnection = true;
                handleConnectionSuccess(*serverProxyPtr, transportAddress);
              },
              [this, serverProxyPtr](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                handleConnectionFailed(*serverProxyPtr, e);
              });
          *serverProxyPtr = gattConsumerEndpoint.createServerProxy(
              handler2peer(btProfile.handler()), btProfile.handle(),
              [this, connectCallback, healthyConnection](shared_ptr<GATTServerProxy> sp,
                                                         GATTDisconnectedReason reason) -> void {
                string reasonText;
                switch (reason) {
                case GATTDisconnectedReason::MY_DISCONNECT:
                  reasonText = "by disconnect request";
                  break;
                case GATTDisconnectedReason::OUT_OF_RANGE:
                  reasonText = "device went out of range";
                  break;
                case GATTDisconnectedReason::PEER_DISCONNECT:
                  reasonText = "device disconnected";
                  break;
                case GATTDisconnectedReason::TIMEOUT:
                  reasonText = "timeout";
                  break;
                case GATTDisconnectedReason::IO_ERROR:
                  reasonText = "I/O error";
                  break;
                }
                logger.DEBUG("DISCONNECTED (" + reasonText + ")");
                if (reason == GATTDisconnectedReason::MY_DISCONNECT) {
                  // We bailed out, so don't attempt a reconnect
                  return;
                } else if (*healthyConnection) {
                  // Attempt to connect again immediately
                  *healthyConnection = false;
                  sp->connect(connectCallback);
                } else {
                  logger.DEBUG("Failed attempt to connect, waiting 2 seconds before retry");
                  if (clock.empty()) {
                    logger.ERROR("No clock found");
                  } else {
                    // Set timer to two seconds
                    timeConsumerEndpoint.startTimer(
                        clock, 2000, false,
                        make_shared<Response<int32_t, bool>>(
                            [sp, connectCallback, healthyConnection](__attribute__((unused)) bool more,
                                                                     __attribute__((unused)) int32_t timerId,
                                                                     bool fired) -> void {
                              if (fired) {
                                *healthyConnection = false;
                                sp->connect(connectCallback);
                              }
                            },
                            [this](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                              logger.ERROR("Couldn't start timer: " + e->message);
                            }));
                  }
                }
              });
          (*serverProxyPtr)->connect(connectCallback);
          return;
        }
      }
    }
  } else {
    logger.ERROR("FOUND A NON-BLUETOOTH DEVICE");
  }
}

void Controller::announceStore(const peer_t &sender) {
  if (configStore.empty())
    configStore = sender;
}

void Controller::announceClock(const peer_t &sender) {
  if (clock.empty())
    clock = sender;
}

void Controller::handleConnectionSuccess(shared_ptr<GATTServerProxy> serverProxy, uint64_t transportAddress) {
  logger.DEBUG("CONNECTED");
  serverProxy->discoverServices(
      nullptr, make_shared<Response<list<shared_ptr<GATTServiceProxy>>>>(
                   [this, serverProxy, transportAddress](__attribute__((unused)) bool more,
                                                         list<shared_ptr<GATTServiceProxy>> services) -> void {
                     for (auto service : services) {
                       // We should only get primary services, but just in case
                       if (!service->primary)
                         continue;
                       if (service->type == GENERIC_ACCESS_SERVICE) {
                         genericAccessSP = service;
                       } else if (service->type == DEVICE_INFORMATION_SERVICE) {
                         deviceInformationSP = service;
                       } else if (service->type == CURRENT_TIME_SERVICE) {
                         currentTimeSP = service;
                       } else if (service->type == GLUCOSE_SERVICE ||
                                  service->type == PULSE_OXIMETER_SERVICE) { // FIXME: no pulseox
                         glucoseSP = service;
                       }
                     }
                     if (genericAccessSP && deviceInformationSP && currentTimeSP && glucoseSP) {
                       // Populate the MDS object
                       logger.DEBUG("Reading MDS...");
                       device = make_shared<MDS>();
                       device->transportId = transportAddress;
                       deviceInformationService.populateMDS(
                           device, deviceInformationSP, genericAccessSP,
                           [this, serverProxy]() -> void { receiveGlucoseObservations(serverProxy); });
                     } else {
                       // Unsupported device
                       logger.ERROR("Unsupported device");
                       // Disconnect and bail
                       serverProxy->disconnect();
                     }
                   },
                   [this, serverProxy](__attribute__((unused)) bool more, shared_ptr<CoreError> e) -> void {
                     logger.ERROR("Connection failed: " + e->message);
                     // Disconnect and bail
                     serverProxy->disconnect();
                   }));
}

void Controller::receiveGlucoseObservations(__attribute__((unused)) shared_ptr<GATTServerProxy> serverProxy) {
  logger.DEBUG("Reading glucose observations");
  // The MDS system type includes Glucose capabilities
  device->systemType.emplace_back(dim::MDS::TypeVer({4113, 2}));

  // Subscribe to glucose observations
  glucoseService.receiveNewObservations(
      glucoseSP, std::to_string(device->systemId),
      [this](set<DIMEntity *> &report) -> void { dim2fhirConverter.convertScanReport(*device, report); });
}

void Controller::handleConnectionFailed(__attribute__((unused)) shared_ptr<GATTServerProxy> serverProxy,
                                        shared_ptr<CoreError> e) {
  // We just print a log message. The disconnected event will trigger a retry after a couple of seconds.
  logger.ERROR(e->message);
}

} // namespace phd_transcoding
} // namespace s4
