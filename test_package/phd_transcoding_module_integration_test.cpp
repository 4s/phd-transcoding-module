#include "phd-transcoding.hpp"

#include "ConfigSettings.pb.h"
#include "DeviceControl.pb.h"
#include "FHIRMeasurement.pb.h"
#include "GATT.pb.h"
#include "PersonalHealthDevice.pb.h"
#include "gtest/gtest.h"
#include <chrono>
#include <thread>

using namespace s4::BasePlate;
using namespace s4::phd_transcoding;
using namespace s4::messages;

class Characteristic {
public:
  Characteristic(int btuuid, int handle, std::vector<string> support) {
    this->uuid = new interfaces::gatt::BTUUID();
    this->uuid->set_short_(btuuid);
    this->handle = handle;
    this->support = support;
  }

  interfaces::gatt::BTUUID *getUuid() { return uuid; }

  int getHandle() { return handle; }

  bool hasRead() { return this->hasProperty("read"); }

  bool hasBroadcast() { return this->hasProperty("broadcast"); }

  bool hasWriteWithoutResponse() { return this->hasProperty("write_without_response"); }

  bool hasWrite() { return this->hasProperty("write"); }

  bool hasNotify() { return this->hasProperty("notify"); }

  bool hasIndicate() { return this->hasProperty("indicate"); }

  bool hasAuthenticatedSignedWrite() { return this->hasProperty("authenticated_signed_write"); }

  bool hasExtendedProperties() { return this->hasProperty("extended_properties"); }

private:
  interfaces::gatt::BTUUID *uuid;
  int handle;
  std::vector<string> support;

  bool hasProperty(std::string property) {
    return std::find(support.begin(), support.end(), property) != support.end();
  }
};

class Service {
public:
  Service(int btuuid, int handle, std::vector<Characteristic> characteristics) {
    this->uuid = new interfaces::gatt::BTUUID();
    this->uuid->set_short_(btuuid);
    this->handle = handle;
    this->characteristics = characteristics;
  }

  interfaces::gatt::BTUUID *getUuid() { return uuid; }

  int getHandle() { return handle; }

  std::vector<Characteristic> getCharacteristics() { return characteristics; }

  bool hasCharacteristicWithHandle(int characteristic_handle) {
    for (Characteristic characteristic : getCharacteristics()) {
      if (characteristic.getHandle() == characteristic_handle) {
        return true;
      }
    }
    return false;
  }

  Characteristic getCharacteristicWithHandle(int characteristic_handle) {
    for (Characteristic characteristic : getCharacteristics()) {
      if (characteristic.getHandle() == characteristic_handle) {
        return characteristic;
      }
    }
    throw new std::invalid_argument("Non existing characteristic");
  }

private:
  interfaces::gatt::BTUUID *uuid;
  int handle;
  std::vector<Characteristic> characteristics;
};

struct Measurement {
  string type, device, observation;
};

class SenderStub : public ModuleBase {
public:
  SenderStub(Context &context) : ModuleBase(context, "00000001") {
    measurement = std::shared_ptr<Measurement>(new Measurement());
    addFunction<interfaces::fhir_measurement::P2C>(
        "FHIRMeasurement.P2C",
        [this](__attribute__((unused)) MetaData metadata, interfaces::fhir_measurement::P2C *msg) -> void {
          if (msg->has_event()) {
            interfaces::fhir_measurement::P2C::Event event = msg->event();
            if (event.has_measurement_received()) {
              interfaces::fhir_measurement::MeasurementReceived measurement_received = event.measurement_received();
              measurement->type = measurement_received.device_type();
              measurement->device = measurement_received.fhir_device();
              measurement->observation = measurement_received.fhir_observation();
              has_measurement = true;
            } else {
              std::cout << "Fhir measurement message without data\n";
            }
          } else {
            std::cout << "Fhir measurement without event\n";
          }
        });
    start();
  }

  void sendSubscribe(int mdcCode) {
    interfaces::personal_health_device::SubscribeDeviceType *deviceType =
        new interfaces::personal_health_device::SubscribeDeviceType();
    deviceType->set_mdc_code(mdcCode);

    interfaces::personal_health_device::C2P::Event *event = new interfaces::personal_health_device::C2P::Event();
    event->set_allocated_subscribe_device_type(deviceType);

    interfaces::personal_health_device::C2P *msg = new interfaces::personal_health_device::C2P();
    msg->set_allocated_event(event);

    sendMulticast("PersonalHealthDevice.C2P", "N/A", msg);
  }

  bool hasMeasurement() { return has_measurement; }

  std::shared_ptr<Measurement> getMeasurement() { return measurement; }

private:
  std::shared_ptr<Measurement> measurement;
  bool has_measurement = false;
};

class DeviceControlStub : public ModuleBase {

public:
  DeviceControlStub(Context &context) : ModuleBase(context, "00000002") {
    addFunction<interfaces::device_control::C2P>(
        "DeviceControl.C2P",
        [this](__attribute__((unused)) MetaData metadata, interfaces::device_control::C2P *msg) -> void {
          if (msg->has_event()) {
            interfaces::device_control::C2P::Event event = msg->event();
            if (event.has_search()) {
              // Announce a fake device
              interfaces::device_control::Search search = event.search();
              searchInitiated(search.search_id());
              announceDevice(search.search_id());
            }
          }
        });
    start();
  }

private:
  void searchInitiated(objId_t search_id) {
    interfaces::device_control::SearchInitiated *search_initiated = new interfaces::device_control::SearchInitiated();
    search_initiated->set_search_id(search_id);

    interfaces::device_control::P2C::Event *event = new interfaces::device_control::P2C::Event();
    event->set_allocated_search_initiated(search_initiated);

    interfaces::device_control::P2C *msg = new interfaces::device_control::P2C();
    msg->set_allocated_event(event);

    sendMulticast("DeviceControl.P2C", "N/A", msg);
  }

  void announceDevice(objId_t search_id) {
    std::cout << "Announcing device\n";
    classes::device::BluetoothDevice *bluetooth_device = new classes::device::BluetoothDevice();
    bluetooth_device->set_bd_addr("00:80:98:01:02:03");
    bluetooth_device->set_name("Contour");

    classes::device::BluetoothDevice_BluetoothProfile *bluetooth_device_profile =
        bluetooth_device->add_bluetooth_profiles();
    bluetooth_device_profile->set_profile(
        classes::device::BluetoothDevice_BluetoothProfile_Profile_GENERIC_ATTRIBUTE_PROFILE);
    // ID of the module
    bluetooth_device_profile->set_handler(3);

    classes::device::DeviceTransport *device_transport = new classes::device::DeviceTransport();
    device_transport->set_allocated_bluetooth_device(bluetooth_device);

    interfaces::device_control::TransportBundle *transport_bundle = new interfaces::device_control::TransportBundle();
    transport_bundle->set_allocated_device_transport(device_transport);

    interfaces::device_control::SearchResult *search_result = new interfaces::device_control::SearchResult();
    search_result->set_allocated_result(transport_bundle);
    search_result->set_search_id(search_id);

    interfaces::device_control::P2C::Event *event = new interfaces::device_control::P2C::Event();
    event->set_allocated_search_result(search_result);

    interfaces::device_control::P2C *msg = new interfaces::device_control::P2C();
    msg->set_allocated_event(event);

    sendMulticast("DeviceControl.P2C", "N/A", msg);
  }
};

class ContourStub : public ModuleBase {
public:
  ContourStub(Context &context) : ModuleBase(context, "00000003") {

    // See bluetooth_utilities.hpp
    this->services.emplace_back(
        0x180a, 10,
        std::vector<Characteristic>({Characteristic(0x2a29, 11, {"read"}), Characteristic(0x2a24, 12, {"read"}),
                                     Characteristic(0x2a25, 13, {"read"}), Characteristic(0x2a23, 14, {"read"})}));
    this->services.emplace_back(
        0x1808, 20,
        std::vector<Characteristic>({Characteristic(0x2a18, 21, {"notify"}), Characteristic(0x2a34, 22, {"notify"}),
                                     Characteristic(0x2a51, 23, {"read"}),
                                     Characteristic(0x2a52, 24, {"indicate", "write"})}));
    this->services.emplace_back(0x1805, 30, std::vector<Characteristic>({}));
    this->services.emplace_back(0x1800, 40, std::vector<Characteristic>({}));

    addFunction<interfaces::gatt::C2P>("GATT.C2P", [this](MetaData metadata, interfaces::gatt::C2P *msg) -> void {
      if (msg->has_request()) {
        interfaces::gatt::C2P::Request request = msg->request();
        if (request.has_connect()) {
          interfaces::gatt::Connect connect = request.connect();
          sendConnectOk(metadata.sender, metadata.callback, connect.gatt_handle());
        } else if (request.has_discover_services()) {
          interfaces::gatt::DiscoverServices discover = request.discover_services();
          sendDiscoverOk(metadata.sender, metadata.callback);
        } else if (request.has_discover_characteristics()) {
          interfaces::gatt::DiscoverCharacteristics discover = request.discover_characteristics();
          sendDiscoverCharacteristicsOk(metadata.sender, metadata.callback, discover);
        } else if (request.has_read_characteristic()) {
          interfaces::gatt::ReadCharacteristic characteristic = request.read_characteristic();
          sendCharacteristic(metadata.sender, metadata.callback, characteristic);
        } else if (request.has_write_characteristic()) {
          interfaces::gatt::WriteCharacteristic write_characteristic = request.write_characteristic();
          handleWriteCharacteristic(metadata.sender, metadata.callback, write_characteristic);
        } else if (request.has_set_notification()) {
          sendNotificationSuccess(metadata.sender, metadata.callback);
        } else if (request.has_write_descriptor()) {
          std::cout << "Unhandled write_descriptor\n";
        } else if (request.has_read_descriptor()) {
          std::cout << "Unhandled read_descriptor\n";
        } else if (request.has_discover_descriptors()) {
          std::cout << "Unhandled discover_descriptors\n";
        } else if (request.has_find_included_services()) {
          std::cout << "Unhandled find_included_services\n";
        } else {
          std::cout << "Got unhandled request\n";
        }
      }
    });
    start();
  }

  void sendMeasurement() {
    interfaces::gatt::Notification *notification = new interfaces::gatt::Notification();
    notification->set_characteristic(21);
    notification->set_value({0x1f, static_cast<char>(sequenceNumber++), 0x00, static_cast<char>(0xe4), 0x07, 0x01, 0x01,
                             0x01, 0x01, 0x01, 0x00, 0x00, 0x2a, static_cast<char>(0xc0), static_cast<char>(0xf1), 0x00,
                             0x00});

    interfaces::gatt::P2C::Event *event = new interfaces::gatt::P2C::Event();
    event->set_allocated_notification(notification);

    interfaces::gatt::P2C *msg = new interfaces::gatt::P2C();
    msg->set_allocated_event(event);

    sendMulticast("GATT.P2C", "N/A", msg);
  }

  bool isConnected() { return connected; }

private:
  std::vector<Service> services;
  int sequenceNumber = 1;
  bool connected = false;

  void handleWriteCharacteristic(peer_t handler, std::string callback,
                                 interfaces::gatt::WriteCharacteristic write_characteristic) {
    if ((int)write_characteristic.value()[0] == 1 && (int)write_characteristic.value()[1] == 6) {
      // [1, 6] indicate that it is a new device, and the transcoder is asking us to send last measurement.
      std::cout << "Write_characteristic: " << write_characteristic.characteristic()
                << " - Send last measurement requested\n";
      sendWriteCharacteristicSuccess(handler, callback, write_characteristic);

      // Sending last recorded measurements, anyone after this will be published
      sendMeasurement();
      connected = true;
    } else {
      std::cout << "Unsupported write characteristic\n";
    }
  }

  void sendWriteCharacteristicSuccess(peer_t handler, std::string callback,
                                      __attribute__((unused))
                                      interfaces::gatt::WriteCharacteristic write_characteristic) {
    interfaces::gatt::WriteCharacteristicSuccess *wcs = new interfaces::gatt::WriteCharacteristicSuccess();
    sendUnicast(handler, "#" + callback, "OK.LAST", wcs);
  }

  void sendNotificationSuccess(peer_t handler, std::string callback) {
    interfaces::gatt::SetNotificationSuccess *ns = new interfaces::gatt::SetNotificationSuccess();
    sendUnicast(handler, "#" + callback, "OK.LAST", ns);
  }

  void sendCharacteristic(peer_t handler, std::string callback, interfaces::gatt::ReadCharacteristic characteristic) {
    std::cout << "Got read of characteristic: " << characteristic.characteristic() << " - #" << callback << "\n";

    int characteristic_handle = characteristic.characteristic();
    interfaces::gatt::ReadCharacteristicSuccess *rcs = new interfaces::gatt::ReadCharacteristicSuccess();

    std::string reply = "";

    for (Service service : this->services) {
      if (service.hasCharacteristicWithHandle(characteristic_handle)) {
        Characteristic ch = service.getCharacteristicWithHandle(characteristic_handle);
        std::cout << "Read characteristic of: " << ch.getUuid()->short_() << "\n";
        int characteristic_uuid = ch.getUuid()->short_();
        if (characteristic_uuid == 10793) {
          // Manufacturer name
          reply = "Some manufacturer";
        }
        if (characteristic_uuid == 10788) {
          // Model number
          reply = "42.42";
        }
        if (characteristic_uuid == 10787) {
          // System ID (8 bytes long)
          reply = "23423534";
        }
        if (characteristic_uuid == 10789) {
          // Serial number string
          reply = "907789683465";
        }
        if (characteristic_uuid == 10833) {
          // GLUCOSE_FEATURE_CHARACTERISTIC (2 bytes long)
          reply = "11";
        }
      }
    }

    std::cout << "reply: " << reply << "\n";
    rcs->set_allocated_value(&reply);
    sendUnicast(handler, "#" + callback, "OK.LAST", rcs);
  }

  void sendConnectOk(peer_t handler, std::string callback, __attribute__((unused)) objId_t handle) {
    std::cout << "Got connect, reply success to: " << handler << " - #" << callback << "\n";
    interfaces::gatt::ConnectSuccess *cs = new interfaces::gatt::ConnectSuccess();
    sendUnicast(handler, "#" + callback, "OK.LAST", cs);
  }

  void sendDiscoverCharacteristicsOk(peer_t handler, std::string callback,
                                     interfaces::gatt::DiscoverCharacteristics discover_characteristics) {
    std::cout << "Got discover characteristics of: " << discover_characteristics.service() << " - #" << callback
              << "\n";
    interfaces::gatt::DiscoverCharacteristicsSuccess *dcs = new interfaces::gatt::DiscoverCharacteristicsSuccess();

    for (Service service : this->services) {
      if (discover_characteristics.service() == service.getHandle()) {
        std::cout << "Sending " << service.getCharacteristics().size()
                  << " characteristics for: " << discover_characteristics.service() << "\n";
        for (Characteristic characteristic : service.getCharacteristics()) {
          interfaces::gatt::GattCharacteristic *gatt_characteristic = dcs->add_characteristics();
          gatt_characteristic->set_allocated_uuid(characteristic.getUuid());
          gatt_characteristic->set_handle(characteristic.getHandle());

          gatt_characteristic->set_read(characteristic.hasRead());
          gatt_characteristic->set_write(characteristic.hasWrite());
          gatt_characteristic->set_indicate(characteristic.hasIndicate());
          gatt_characteristic->set_broadcast(characteristic.hasBroadcast());
          gatt_characteristic->set_write_without_response(characteristic.hasWriteWithoutResponse());
          gatt_characteristic->set_notify(characteristic.hasNotify());
          gatt_characteristic->set_authenticated_signed_write(characteristic.hasAuthenticatedSignedWrite());
          gatt_characteristic->set_extended_properties(characteristic.hasExtendedProperties());
        }
      }
    }
    sendUnicast(handler, "#" + callback, "OK.LAST", dcs);
  }

  void sendDiscoverOk(peer_t handler, std::string callback) {
    std::cout << "Got discover, reply success to: " << handler << " - #" << callback << "\n";
    interfaces::gatt::DiscoverServicesSuccess *dss = new interfaces::gatt::DiscoverServicesSuccess();

    for (Service service : this->services) {
      interfaces::gatt::GattService *gatt_service = dss->add_services();
      gatt_service->set_primary(true);
      gatt_service->set_allocated_uuid(service.getUuid());
      gatt_service->set_handle(service.getHandle());
    }
    sendUnicast(handler, "#" + callback, "OK.LAST", dss);
  }
};

class LoggingModule : public AbstractLogModule, public virtual ModuleBase {
public:
  LoggingModule(Context &context) : ModuleBase(context, "00000004"), AbstractLogModule(classes::log::DEBUG) { start(); }

  virtual void fatal(peer_t moduleId, std::string message, std::string file, int line) {
    log(moduleId, message, file, line);
  }

  virtual void error(peer_t moduleId, std::string message, std::string file, int line) {
    log(moduleId, message, file, line);
  }

  virtual void warning(peer_t moduleId, std::string message, std::string file, int line) {
    log(moduleId, message, file, line);
  }

  virtual void information(peer_t moduleId, std::string message, std::string file, int line) {
    log(moduleId, message, file, line);
  }

  virtual void debug(peer_t moduleId, std::string message, std::string file, int line) {
    log(moduleId, message, file, line);
  }

private:
  void log(__attribute__((unused)) peer_t moduleId, std::string message, __attribute__((unused)) std::string file,
           __attribute__((unused)) int line) {
    std::cout << "LOG: " << message << "\n";
  }
};

class ConfigStoreModule : public ModuleBase {
public:
  ConfigStoreModule(Context &context) : ModuleBase(context, "00000005") {
    addFunction<interfaces::config_settings::C2P>(
        "ConfigSettings.C2P", [this](MetaData metadata, interfaces::config_settings::C2P *msg) -> void {
          if (msg->has_event()) {
            interfaces::config_settings::C2P::Event event = msg->event();
            if (event.has_solicit_store()) {
              announceStore();
            } else {
              std::cout << "Unhandled config event"
                        << "\n";
            }
          }
          if (msg->has_request()) {
            interfaces::config_settings::C2P::Request request = msg->request();
            if (request.has_read_configuration()) {
              interfaces::config_settings::ReadConfiguration read_configuration = request.read_configuration();
              sendConfiguration(metadata.sender, metadata.callback, read_configuration);
            } else if (request.has_write_configuration()) {
              // no-op at this point
            } else {
              std::cout << "Unhandled config request"
                        << "\n";
            }
          }
        });
    start();
  }

private:
  void sendConfiguration(peer_t handler, std::string callback,
                         interfaces::config_settings::ReadConfiguration read_configuration) {
    std::cout << "CS: got request for: " << read_configuration.configuration_name() << "\n";
    interfaces::config_settings::ReadConfigurationSuccess *rcs =
        new interfaces::config_settings::ReadConfigurationSuccess();

    interfaces::config_settings::KeyValuePair *kvp = rcs->add_configuration();
    if (read_configuration.configuration_name() == "phd_transcoding_glucose_sequence_numbers") {
      // TODO: This is not really used
      kvp->set_key("key");
      kvp->set_value("value");
    } else {
      std::cout << "CS: got unknown request"
                << "\n";
    }

    sendUnicast(handler, "#" + callback, "OK.LAST", rcs);
  }

  void announceStore() {
    std::cout << "Announcing store"
              << "\n";
    interfaces::config_settings::AnnounceStore *announce_store = new interfaces::config_settings::AnnounceStore();

    interfaces::config_settings::P2C::Event *event = new interfaces::config_settings::P2C::Event();
    event->set_allocated_announce_store(announce_store);

    interfaces::config_settings::P2C *msg = new interfaces::config_settings::P2C();
    msg->set_allocated_event(event);

    sendMulticast("ConfigSettings.P2C", "N/A", msg);
  }
};

bool await(std::function<bool()> predicate) {
  int current_iteration = 1;
  int max_iterations = 100;
  int milliseconds_sleep = 10;

  do {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds_sleep));
    if (current_iteration++ >= max_iterations) {
      return false;
    }
  } while (!predicate());

  return true;
}

bool s1ContainsS2(std::string s1, std::string s2) { return s1.find(s2) != std::string::npos; }

Context *context;
MasterModule *master;
Reflector *reflector;
ModuleBase *transcoder;
SenderStub *senderStub;
ContourStub *contourStub;
DeviceControlStub *deviceControlStub;
LoggingModule *loggingModule;
ConfigStoreModule *configStoreModule;

void setup() {
  context = new Context();
  reflector = new Reflector(context);
  transcoder = new PHDTranscodingModule(*context);
  senderStub = new SenderStub(*context);
  contourStub = new ContourStub(*context);
  deviceControlStub = new DeviceControlStub(*context);
  loggingModule = new LoggingModule(*context);
  configStoreModule = new ConfigStoreModule(*context);
  master = new MasterModule(*context, 7);

  reflector->start();

  if (!await([&] { return transcoder->getApplicationState() == RUNNING; })) {
    master->requestShutdown(true);
    FAIL() << "transcoder module did not go into running state";
  }
}

void shutdown() {
  master->requestShutdown(true);

  if (!await([&] { return transcoder->getApplicationState() == FINALIZING; })) {
    master->requestShutdown(true);
    FAIL() << "transcoder module did not shutdown";
  }

  delete transcoder;
  transcoder = nullptr;
  delete senderStub;
  senderStub = nullptr;
  delete master;
  master = nullptr;
  delete reflector;
  reflector = nullptr;
  delete contourStub;
  contourStub = nullptr;
  delete deviceControlStub;
  deviceControlStub = nullptr;
  delete loggingModule;
  loggingModule = nullptr;
  delete configStoreModule;
  configStoreModule = nullptr;
  delete context;
  context = nullptr;
}

TEST(PhdTranscoder, startup_and_shutdown) {
  setup();
  shutdown();
}

TEST(PhdTranscoder, initiate_subscription) {
  setup();
  senderStub->sendSubscribe(528401);

  if (!await([&] { return contourStub->isConnected(); })) {
    master->requestShutdown(true);
    FAIL() << "Contour stub did not connect";
  }

  contourStub->sendMeasurement();

  if (!await([&] { return senderStub->hasMeasurement(); })) {
    master->requestShutdown(true);
    FAIL() << "No measurement was received";
  }

  std::shared_ptr<Measurement> measurement = senderStub->getMeasurement();

  ASSERT_TRUE(measurement->type == "528401");

  ASSERT_TRUE(s1ContainsS2(measurement->device, "\"manufacturer\": \"Some manufacturer\""));
  ASSERT_TRUE(s1ContainsS2(measurement->device, "\"modelNumber\": \"42.42\""));
  ASSERT_TRUE(s1ContainsS2(measurement->device, "\"serialNumber\": \"907789683465\""));

  ASSERT_TRUE(s1ContainsS2(measurement->observation, "\"value\": 4.2"));
  ASSERT_TRUE(s1ContainsS2(measurement->observation, "\"code\": \"mmol/L\""));

  shutdown();
}
