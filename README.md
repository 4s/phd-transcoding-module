Personal Health Device Transcoding  module
==========================================
This module is an implementation of the Personal
Health Device Transcoding white paper, available from
https://www.bluetooth.com/bluetooth-resources/personal-health-devices-transcoding/

Only parts of the implementation is finished at this point.

This module should be split into several modules, one translating
from the GATT device to the IEEE domain information model and one
for translating generic DIM objects to a FHIR bundle and/or FHIR
server upload.

Building
--------
Our C++ based projects are build using conan (conan.io) and dependencies
are pulled from bintray using conan.

It is possible to build using standard conan commands.

Dependencies
------------
The transcoding module require phg-messages to produce messages, and the
phg-native-baseplate to send messages to.

These dependencies are pulled automatically by conan.

Publishing
----------
We have created a script that can be called to build and publish to 
bintray as 4s:
```bash
./publish
```
The variables BT_PASSWORD and BT_USER needs to be set for the script to 
work.

Compiling requires conan, android-sdk and ndk, these tools has been installed
in a docker-hub image for convnenience:
```
alexjesper/conan-android-ndk
```
The version of the package is defined in the conan file.

Using
-----
Starting the module, and the corresponding platform on a mobile platform
is framework dependent, previously cordova has been used, and a wrapper
project that includes this in the cordova build and starts the module
has been created (https://bitbucket.org/4s/cdvw-phd-transcoding)

Issue tracking
--------------
If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.

License
-------
The source files are released under Apache 2.0, you can obtain a
copy of the License at: http://www.apache.org/licenses/LICENSE-2.0