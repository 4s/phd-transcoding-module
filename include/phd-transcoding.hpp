/*
 *   Copyright 2020 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * @file
 * @brief Personal Health Device Transcoding module.
 *
 * This module should be split into several modules, one translating
 * from the GATT device to the IEEE domain information model and one
 * for translating generic DIM objects to a FHIR bundle and/or FHIR
 * server upload.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2020 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef PHD_TRANSCODING_HPP
#define PHD_TRANSCODING_HPP

#include "BasePlate-fix.hpp"

#include <memory>

/**
 * @defgroup phdtranscoding PHD transcoding module
 * @ingroup group_core
 * @brief Transcoder for Bluetooth LE-based PHD communication.
 *
 * It should be split into several modules, one translating from the
 * GATT device to the IEEE domain information model and one for
 * translating generic DIM objects to a FHIR bundle and/or FHIR server
 * upload.
 */
namespace s4 {
namespace phd_transcoding {

/**
 * @ingroup phdtranscoding
 * @brief Personal Health Device transcoder module
 *
 * This module should be split into several modules, one translating
 * from the GATT device to the IEEE domain information model and one
 * for translating generic DIM objects to a FHIR bundle and/or FHIR
 * server upload.
 */
class PHDTranscodingModule final : public BasePlate::ModuleBase {

public:
  explicit PHDTranscodingModule(BasePlate::Context &context);

private:
  /// @brief Opaque wrapper of the private parts.
  class PImpl;

  /// @brief Container of the private parts.
  std::unique_ptr<PImpl> pImpl;
};
} // namespace phd_transcoding
} // namespace s4

#endif // PHD_TRANSCODING_HPP
