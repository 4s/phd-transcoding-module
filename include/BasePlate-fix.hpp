
// FIXME: This header temporarily handles some missing baseplate
// functionality. Must be removed when the baseplate is finalized.

#ifndef BASEPLATEFIX_H
#define BASEPLATEFIX_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include "BasePlate.hpp"
#pragma GCC diagnostic pop

namespace s4 {
namespace BasePlate {

typedef uint64_t objId_t;

class Logger {
public:
  virtual void fatal(const std::string &file, int line, const std::string &message) = 0;
  virtual void error(const std::string &file, int line, const std::string &message) = 0;
  // virtual void warn(const std::string &file, int line, const std::string &message) =0;
  // virtual void info(const std::string &file, int line, const std::string &message) =0;
  virtual void debug(const std::string &file, int line, const std::string &message) = 0;
  virtual objId_t createObjId() = 0;
  virtual ~Logger() = default;
};

struct CoreError {
  std::string interfaceName;
  int32_t errorCode = 0;
  const std::string moduleTag;
  const std::string message;
  std::string details;
  std::string fileName;
  int32_t lineNumber = 0;
  std::string className;
  std::string functionName;
  std::shared_ptr<CoreError> causedBy = nullptr;
  CoreError(const std::string &moduleTag, const std::string &message) : moduleTag(moduleTag), message(message) {}
  CoreError(const std::string &moduleTag, const std::string &message, const std::string &interfaceName,
            int32_t errorCode)
      : interfaceName(interfaceName), errorCode(errorCode), moduleTag(moduleTag), message(message) {}
};

class EndpointFactoryTemporary {
public:
  template <class A> void addFunction(std::string name, std::function<void(MetaData, A *)> function) {
    addFunction(name, new Consumer<A>(function));
  }
  template <class A>
  void sendUnicastWithCallback(const peer_t &destination, const std::string &interface,
                               std::function<void(MetaData, A *)> callback,
                               std::function<void(MetaData, std::shared_ptr<CoreError>)> errorCallback,
                               google::protobuf::MessageLite *data = nullptr) {

    std::string uid = registerResponseUID(interface, new Consumer<A>(callback), errorCallback);
    sendUnicastWithCallback(destination, interface, "N/A", uid, data);
  }
  virtual void removeFunction(std::string interface) = 0;
  virtual ModuleBase &getParent() = 0;

private:
  virtual std::string registerResponseUID(std::string interface, Function *successCallback,
                                          std::function<void(MetaData, std::shared_ptr<CoreError>)> errorCallback) = 0;
  virtual void addFunction(std::string, Function *function) = 0;
  virtual void sendUnicastWithCallback(peer_t destination, std::string interface, std::string signal,
                                       std::string callbackInterface,
                                       google::protobuf::MessageLite *arg = nullptr) const = 0;
};

template <class... T> class Response {
private:
  std::function<void(bool, T...)> _success;
  std::function<void(bool, std::shared_ptr<CoreError>)> _error;

public:
  Response(std::function<void(bool, T...)> success, std::function<void(bool, std::shared_ptr<CoreError>)> error)
      : _success(success), _error(error) {}
  void success(bool more, T... results) { _success(more, results...); }
  void error(bool more, std::shared_ptr<CoreError> e) { _error(more, e); }
};

peer_t handler2peer(int32_t handler);

} // namespace BasePlate
} // namespace s4

#define FATAL(message) fatal(__FILE__, __LINE__, message)
#define ERROR(message) error(__FILE__, __LINE__, message)
//#define WARN(message) warn(__FILE__, __LINE__, message)
//#define INFO(message) info(__FILE__, __LINE__, message)
#define DEBUG(message) debug(__FILE__, __LINE__, message)

#endif // BASEPLATEFIX_H
